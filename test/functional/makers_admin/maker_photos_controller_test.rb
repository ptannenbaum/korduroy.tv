

require 'test_helper'

class MakersAdmin::MakerPhotosControllerTest < ActionController::TestCase
  setup do
    @makers_admin_maker_photo = makers_admin_maker_photos(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:makers_admin_maker_photos)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create makers_admin_maker_photo" do
    assert_difference('MakersAdmin::MakerPhoto.count') do
      post :create, makers_admin_maker_photo: @makers_admin_maker_photo.attributes
    end

    assert_redirected_to makers_admin_maker_photo_path(assigns(:makers_admin_maker_photo))
  end

  test "should show makers_admin_maker_photo" do
    get :show, id: @makers_admin_maker_photo.to_param
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @makers_admin_maker_photo.to_param
    assert_response :success
  end

  test "should update makers_admin_maker_photo" do
    put :update, id: @makers_admin_maker_photo.to_param, makers_admin_maker_photo: @makers_admin_maker_photo.attributes
    assert_redirected_to makers_admin_maker_photo_path(assigns(:makers_admin_maker_photo))
  end

  test "should destroy makers_admin_maker_photo" do
    assert_difference('MakersAdmin::MakerPhoto.count', -1) do
      delete :destroy, id: @makers_admin_maker_photo.to_param
    end

    assert_redirected_to makers_admin_maker_photos_path
  end
end
