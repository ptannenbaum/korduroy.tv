

require 'test_helper'

class MakersAdmin::MakersControllerTest < ActionController::TestCase
  setup do
    @makers_admin_maker = makers_admin_makers(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:makers_admin_makers)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create makers_admin_maker" do
    assert_difference('MakersAdmin::Maker.count') do
      post :create, makers_admin_maker: @makers_admin_maker.attributes
    end

    assert_redirected_to makers_admin_maker_path(assigns(:makers_admin_maker))
  end

  test "should show makers_admin_maker" do
    get :show, id: @makers_admin_maker.to_param
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @makers_admin_maker.to_param
    assert_response :success
  end

  test "should update makers_admin_maker" do
    put :update, id: @makers_admin_maker.to_param, makers_admin_maker: @makers_admin_maker.attributes
    assert_redirected_to makers_admin_maker_path(assigns(:makers_admin_maker))
  end

  test "should destroy makers_admin_maker" do
    assert_difference('MakersAdmin::Maker.count', -1) do
      delete :destroy, id: @makers_admin_maker.to_param
    end

    assert_redirected_to makers_admin_makers_path
  end
end
