

require 'test_helper'

class MakersAdmin::ImagesControllerTest < ActionController::TestCase
  setup do
    @makers_admin_image = makers_admin_images(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:makers_admin_images)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create makers_admin_image" do
    assert_difference('MakersAdmin::Image.count') do
      post :create, makers_admin_image: @makers_admin_image.attributes
    end

    assert_redirected_to makers_admin_image_path(assigns(:makers_admin_image))
  end

  test "should show makers_admin_image" do
    get :show, id: @makers_admin_image.to_param
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @makers_admin_image.to_param
    assert_response :success
  end

  test "should update makers_admin_image" do
    put :update, id: @makers_admin_image.to_param, makers_admin_image: @makers_admin_image.attributes
    assert_redirected_to makers_admin_image_path(assigns(:makers_admin_image))
  end

  test "should destroy makers_admin_image" do
    assert_difference('MakersAdmin::Image.count', -1) do
      delete :destroy, id: @makers_admin_image.to_param
    end

    assert_redirected_to makers_admin_images_path
  end
end
