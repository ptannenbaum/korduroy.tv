

require 'test_helper'

class MakersAdmin::BlogsControllerTest < ActionController::TestCase
  setup do
    @makers_admin_blog = makers_admin_blogs(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:makers_admin_blogs)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create makers_admin_blog" do
    assert_difference('MakersAdmin::Blog.count') do
      post :create, makers_admin_blog: @makers_admin_blog.attributes
    end

    assert_redirected_to makers_admin_blog_path(assigns(:makers_admin_blog))
  end

  test "should show makers_admin_blog" do
    get :show, id: @makers_admin_blog.to_param
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @makers_admin_blog.to_param
    assert_response :success
  end

  test "should update makers_admin_blog" do
    put :update, id: @makers_admin_blog.to_param, makers_admin_blog: @makers_admin_blog.attributes
    assert_redirected_to makers_admin_blog_path(assigns(:makers_admin_blog))
  end

  test "should destroy makers_admin_blog" do
    assert_difference('MakersAdmin::Blog.count', -1) do
      delete :destroy, id: @makers_admin_blog.to_param
    end

    assert_redirected_to makers_admin_blogs_path
  end
end
