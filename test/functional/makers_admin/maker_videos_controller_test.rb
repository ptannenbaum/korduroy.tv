

require 'test_helper'

class MakersAdmin::MakerVideosControllerTest < ActionController::TestCase
  setup do
    @makers_admin_maker_video = makers_admin_maker_videos(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:makers_admin_maker_videos)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create makers_admin_maker_video" do
    assert_difference('MakersAdmin::MakerVideo.count') do
      post :create, makers_admin_maker_video: @makers_admin_maker_video.attributes
    end

    assert_redirected_to makers_admin_maker_video_path(assigns(:makers_admin_maker_video))
  end

  test "should show makers_admin_maker_video" do
    get :show, id: @makers_admin_maker_video.to_param
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @makers_admin_maker_video.to_param
    assert_response :success
  end

  test "should update makers_admin_maker_video" do
    put :update, id: @makers_admin_maker_video.to_param, makers_admin_maker_video: @makers_admin_maker_video.attributes
    assert_redirected_to makers_admin_maker_video_path(assigns(:makers_admin_maker_video))
  end

  test "should destroy makers_admin_maker_video" do
    assert_difference('MakersAdmin::MakerVideo.count', -1) do
      delete :destroy, id: @makers_admin_maker_video.to_param
    end

    assert_redirected_to makers_admin_maker_videos_path
  end
end
