

require 'test_helper'

class Admin::SupportersControllerTest < ActionController::TestCase
  setup do
    @admin_supporter = admin_supporters(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:admin_supporters)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create admin_supporter" do
    assert_difference('Admin::Supporter.count') do
      post :create, admin_supporter: @admin_supporter.attributes
    end

    assert_redirected_to admin_supporter_path(assigns(:admin_supporter))
  end

  test "should show admin_supporter" do
    get :show, id: @admin_supporter.to_param
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @admin_supporter.to_param
    assert_response :success
  end

  test "should update admin_supporter" do
    put :update, id: @admin_supporter.to_param, admin_supporter: @admin_supporter.attributes
    assert_redirected_to admin_supporter_path(assigns(:admin_supporter))
  end

  test "should destroy admin_supporter" do
    assert_difference('Admin::Supporter.count', -1) do
      delete :destroy, id: @admin_supporter.to_param
    end

    assert_redirected_to admin_supporters_path
  end
end
