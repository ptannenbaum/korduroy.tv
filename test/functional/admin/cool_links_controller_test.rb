

require 'test_helper'

class Admin::CoolLinksControllerTest < ActionController::TestCase
  setup do
    @admin_cool_link = admin_cool_links(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:admin_cool_links)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create admin_cool_link" do
    assert_difference('Admin::CoolLink.count') do
      post :create, admin_cool_link: @admin_cool_link.attributes
    end

    assert_redirected_to admin_cool_link_path(assigns(:admin_cool_link))
  end

  test "should show admin_cool_link" do
    get :show, id: @admin_cool_link.to_param
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @admin_cool_link.to_param
    assert_response :success
  end

  test "should update admin_cool_link" do
    put :update, id: @admin_cool_link.to_param, admin_cool_link: @admin_cool_link.attributes
    assert_redirected_to admin_cool_link_path(assigns(:admin_cool_link))
  end

  test "should destroy admin_cool_link" do
    assert_difference('Admin::CoolLink.count', -1) do
      delete :destroy, id: @admin_cool_link.to_param
    end

    assert_redirected_to admin_cool_links_path
  end
end
