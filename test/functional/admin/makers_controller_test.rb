

require 'test_helper'

class Admin::MakersControllerTest < ActionController::TestCase
  setup do
    @admin_maker = admin_makers(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:admin_makers)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create admin_maker" do
    assert_difference('Admin::Maker.count') do
      post :create, admin_maker: @admin_maker.attributes
    end

    assert_redirected_to admin_maker_path(assigns(:admin_maker))
  end

  test "should show admin_maker" do
    get :show, id: @admin_maker.to_param
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @admin_maker.to_param
    assert_response :success
  end

  test "should update admin_maker" do
    put :update, id: @admin_maker.to_param, admin_maker: @admin_maker.attributes
    assert_redirected_to admin_maker_path(assigns(:admin_maker))
  end

  test "should destroy admin_maker" do
    assert_difference('Admin::Maker.count', -1) do
      delete :destroy, id: @admin_maker.to_param
    end

    assert_redirected_to admin_makers_path
  end
end
