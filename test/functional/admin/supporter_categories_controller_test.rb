

require 'test_helper'

class Admin::SupporterCategoriesControllerTest < ActionController::TestCase
  setup do
    @admin_supporter_category = admin_supporter_categories(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:admin_supporter_categories)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create admin_supporter_category" do
    assert_difference('Admin::SupporterCategory.count') do
      post :create, admin_supporter_category: @admin_supporter_category.attributes
    end

    assert_redirected_to admin_supporter_category_path(assigns(:admin_supporter_category))
  end

  test "should show admin_supporter_category" do
    get :show, id: @admin_supporter_category.to_param
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @admin_supporter_category.to_param
    assert_response :success
  end

  test "should update admin_supporter_category" do
    put :update, id: @admin_supporter_category.to_param, admin_supporter_category: @admin_supporter_category.attributes
    assert_redirected_to admin_supporter_category_path(assigns(:admin_supporter_category))
  end

  test "should destroy admin_supporter_category" do
    assert_difference('Admin::SupporterCategory.count', -1) do
      delete :destroy, id: @admin_supporter_category.to_param
    end

    assert_redirected_to admin_supporter_categories_path
  end
end
