

require 'test_helper'

class MakerContactsControllerTest < ActionController::TestCase
  setup do
    @maker_contact = maker_contacts(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:maker_contacts)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create maker_contact" do
    assert_difference('MakerContact.count') do
      post :create, maker_contact: @maker_contact.attributes
    end

    assert_redirected_to maker_contact_path(assigns(:maker_contact))
  end

  test "should show maker_contact" do
    get :show, id: @maker_contact.to_param
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @maker_contact.to_param
    assert_response :success
  end

  test "should update maker_contact" do
    put :update, id: @maker_contact.to_param, maker_contact: @maker_contact.attributes
    assert_redirected_to maker_contact_path(assigns(:maker_contact))
  end

  test "should destroy maker_contact" do
    assert_difference('MakerContact.count', -1) do
      delete :destroy, id: @maker_contact.to_param
    end

    assert_redirected_to maker_contacts_path
  end
end
