tinyMCEPopup.requireLangPack();

var VimeoDialog = {
	init : function() {},

	insert : function() {
		
		var id = document.forms[0].VimeoID.value;
		
		if (id) {
			var matches = id.match(/vimeo\.com\/(\d+)&?/i);
			matches = matches[1].split('&');
			id = matches[0];
			
			// Insert the contents from the input into the document
		
			//var embedCode = '<object width="400" height="225"><param name="allowfullscreen" value="true" /><param name="allowscriptaccess" value="always" /><param name="movie" value="http://vimeo.com/moogaloop.swf?clip_id='+id+'&amp;server=vimeo.com&amp;show_title=1&amp;show_byline=1&amp;show_portrait=1&amp;color=&amp;fullscreen=1&amp;autoplay=0&amp;loop=0" /><embed src="http://vimeo.com/moogaloop.swf?clip_id='+id+'&amp;server=vimeo.com&amp;show_title=1&amp;show_byline=1&amp;show_portrait=1&amp;color=&amp;fullscreen=1&amp;autoplay=0&amp;loop=0" type="application/x-shockwave-flash" allowfullscreen="true" allowscriptaccess="always" width="400" height="225"></embed></object>';
			var embedCode = '<iframe src="http://player.vimeo.com/video/'+id+'?title=0&amp;byline=0&amp;portrait=0&amp;color=ffffff" width="651" height="366" frameborder="0" webkitAllowFullScreen allowFullScreen></iframe>'
			tinyMCEPopup.editor.execCommand('mceInsertRawHTML', false, embedCode);
			tinyMCEPopup.close();
		}
	}
};

tinyMCEPopup.onInit.add(VimeoDialog.init, VimeoDialog);
