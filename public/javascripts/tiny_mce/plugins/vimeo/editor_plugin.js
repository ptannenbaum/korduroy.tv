(function() {
  tinymce.PluginManager.requireLangPack("vimeo");
  tinymce.create("tinymce.plugins.VimeoPlugin", {init:function(a, b) {
    a.addCommand("mceVimeo", function() {
      a.windowManager.open({file:b + "/dialog.htm", width:320 , height:120 , inline:1}, {plugin_url:b})
    });
    a.addButton("vimeo", {title:"vimeo.desc", cmd:"mceVimeo", image:b + "/img/vimeo.gif"});
    a.onNodeChange.add(function(e, c, d) {
      c.setActive("vimeo", d.nodeName == "IMG")
    })
  }, createControl:function() {
    return null
  }, getInfo:function() {
    return{longname:"Vimeo plugin", author:"Caleb Hale", authorurl:"http://cjhale.co.uk/", infourl:"http://cjhale.co.uk/", version:"1.0"}
  }});
  tinymce.PluginManager.add("vimeo", tinymce.plugins.VimeoPlugin)
})();