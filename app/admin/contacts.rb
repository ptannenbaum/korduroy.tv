ActiveAdmin.register Contact do
  menu :label => "Contacts", :parent => "Misc", :if => proc { can?(:read, Contact)}
  
  index do
    column :name
    column :email
    column :created_at
    default_actions
  end
  
  form do |f|
    f.inputs do
      f.input :name
      f.input :email
      f.input :message
    end
    f.buttons
  end
  
  controller do
    def create
      @contact = Contact.new(params[:contact])
      
      @contact.ip = request.remote_ip
      
      respond_to do |format|
        if @contact.save
          format.html { redirect_to admin_contacts_path, notice: 'Contact was successfully created.' }
          format.json { render json: @contact, status: :created, location: @contact }
        else
          format.html { render action: "new" }
          format.json { render json: @contact.errors, status: :unprocessable_entity }
        end
      end
    end
  end
end
