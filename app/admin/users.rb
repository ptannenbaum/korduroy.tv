ActiveAdmin.register User do
  menu :if => proc { can?(:read, User)}
  
  controller do
    def create
      @user = User.new(params[:user])
      
      @user.maker_id        = params[:user][:maker_id]
      @user.admin           = params[:user][:admin]
      @user.supplier        = params[:user][:supplier]
      @user.default_blogger = params[:user][:default_blogger]
      
      respond_to do |format|
        if @user.save
          format.html { redirect_to admin_users_path, notice: 'User was successfully created.' }
          format.json { render json: @user, status: :created, location: @user }
        else
          format.html { render action: "new" }
          format.json { render json: @user.errors, status: :unprocessable_entity }
        end
      end
    end
    
    def update
      @user = User.find(params[:id])
      
      # Remove the parameters if they aren't set so we can update
      # users without changing their passwords
      params[:user].delete :password if params[:user][:password].blank?
      params[:user][:password_confirmation].blank? if params[:user].delete :password_confirmation
      
      @user.attributes = params[:user]
      
      # we are explicitly setting these because they should only be set
      # by admins
      @user.maker_id        = params[:user][:maker_id]
      @user.admin           = params[:user][:admin]
      @user.supplier        = params[:user][:supplier]
      @user.default_blogger = params[:user][:default_blogger]

      respond_to do |format|
        if @user.save
          format.html { redirect_to admin_users_path, notice: 'User was successfully updated.' }
          format.json { head :ok }
        else
          format.html { render action: "edit" }
          format.json { render json: @user.errors, status: :unprocessable_entity }
        end
      end
    end
  end
  
  index do
    column :id
    column :avatar do |user|
      if user.avatar?
        image_tag(user.avatar.small)
      else
        image_tag("/assets/avatar_small.png")
      end
    end
    column :name
    column :email do |user|
      mail_to user.email, user.email
    end
    column :admin?
    column :is_maker?
    column :supplier?, :label => "Supporter"
    default_actions
  end
  
  form :html => { :enctype => "multipart/form-data" } do |f|
    f.inputs do
      f.input :first_name
      f.input :last_name
      f.input :email
      if f.object.new_record? || f.object.avatar.blank?
        f.input :avatar, :as => :file
      else
        f.input :avatar, :as => :file, :hint => f.template.image_tag(f.object.avatar.large.url, :class => "avatar")
        f.input :avatar_cache, :as => :hidden
      end
      f.input :maker #TODO: This should be changed so that it only shows Makers who haven't already been assigned to a User
      #f.input :maker_id, :label => 'Maker ID'
    end
    f.inputs 'Social Media/Blogs' do
      f.input :twitter
      f.input :facebook
      f.input :flickr
      f.input :pinterest
      f.input :tumbler, :label => "Tumblr"
      f.input :wordpress
      f.input :blogger
    end
    f.inputs 'Security' do
      f.input :password
      f.input :password_confirmation
      f.input :admin, :label => 'Admin?'
      f.input :supplier, :label => 'Supporter?'
      f.input :default_blogger, :label => 'Default blogger?'
    end
    f.buttons
  end
end
