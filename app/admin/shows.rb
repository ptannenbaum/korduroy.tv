ActiveAdmin.register Show do
  menu :label => "Episodes", :parent => "Shows", :if => proc { can?(:read, Show)}
  
  scope :all, :default => true
  scope :staff_pick
  
  controller do
    def create
      @show = Show.new(params[:show])
      
      @show.comments.each { |comment| comment.user = current_user if comment.new_record? }
      
      if @show.thumbnail.blank?
        @v = Vimeo::Simple::Video.info(@show.vimeo)
        @show.thumbnail = @v[0]['thumbnail_medium']
        #@show.poster = @v[0]['thumbnail_large'].gsub("_640.", "_960.")
      end

      respond_to do |format|
        if @show.save
          format.html { redirect_to admin_shows_path, notice: 'Show was successfully created.' }
          format.json { render json: @show, status: :created, location: @show }
        else
          format.html { render action: "new" }
          format.json { render json: @show.errors, status: :unprocessable_entity }
        end
      end
    end
    
    def update
      @show = Show.find(params[:id])
      @show.attributes = params[:show]
      @show.comments.each { |comment| comment.user = current_user if comment.new_record? }
      # The has_many :through association isn't deleted if the user
      # deselects all makers so we will delete them here.
      @show.makers.delete_all if params[:show][:maker_ids].blank?
      
      if @show.thumbnail.blank?
        @v = Vimeo::Simple::Video.info(@show.vimeo)
        @show.thumbnail = @v[0]['thumbnail_medium']
      end
      
      #if @show.poster.blank?
      #  @v = Vimeo::Simple::Video.info(@show.vimeo)
      #  @show.poster = @v[0]['thumbnail_large'].gsub("_640.", "_960.")
      #end
      
      respond_to do |format|
        if @show.save
          format.html { redirect_to admin_shows_path, notice: 'Show was successfully updated.' }
          format.json { head :ok }
        else
          format.html { render action: "edit" }
          format.json { render json: @show.errors, status: :unprocessable_entity }
        end
      end
    end
  end
  
  form :html => { :enctype => "multipart/form-data" } do |f|
    f.inputs do
      f.input :show_category
      if f.object.new_record?
        f.input :title, :input_html => { :class => 'slug_input' }
      else
        f.input :title
      end
      f.input :slug, :input_html => { :class => 'slug' }
      f.input :staff_pick
      f.input :description, :input_html => {  :rows => 6 }
      #f.input :tag_list, :as => :check_boxes, :collection => ["Apparel", "Art", "Bodysurfing", "Books", "Camping", "Contests", "Craft", "Environment", "Events", "Filmmaking", "Finless", "Fishes", "Food", "Garden", "Health", "Heshians", "Hippies", "Home", "Jocks", "Longboards", "Lumberjacks", "Outdoors", "Organizations", "Photography", "Preppies", "Shaping", "Shortboards", "Skate", "Snow", "Technology", "Travel", "Trailers", "Wood"]
      f.input :makers, :as => :select, :label => "Makers featured in episode"
      f.input :comments_closed
      f.input :created_at, :ampm => true
    end
    
    # tags
    f.inputs :name => "Tags" do
      f.input :tag_list, :label => "People", :as => :check_boxes, :collection => ["Kelly Slater", "Ryan Burch", "Ryan Tatar", "Cyrus Sutton", "Ryan Lovelace", "Danny Hess", "Richard Kenvin", "Dane Reynolds", "Derek Dunfee", "Tyler Warren", "Neal Purchase", "Daniel Thompson", "Dave Allee", "Wegener", "Adrian Knott", "Derek Hynd", "Ed Sloane", "Steve Pendarvis", "Jeremy Rumas", "Jack Coleman"]
      f.input :tag_list, :label => "Places", :as => :check_boxes, :collection => ["Canada", "Australia", "Japan", "Europe", "Spain", "UK", "Mexico", "Indo", "New York", "Brazil", "West Oz", "Central America", "South America", "East coast US", "South Pacific", "So Cal", "Nor Cal", "beach break", "point break", "San Diego", "Hawaii", "New Zealand", "Africa", "Central California"]
      f.input :tag_list, :label => "Surfcrafts", :as => :check_boxes, :collection => ["handplane", "alaia", "paipo", "asymmetrics", "kneeboarding", "prone surfing", "2+1", "noseriding", "thruster", "quad", "single fin", "twin fin", "EPS", "Finless", "Longboards", "Shortboards", "hull", "lord board", "bodyboard", "Pendoflex"]
      f.input :tag_list, :label => "Business", :as => :check_boxes, :collection => ["time management", "industry"]
      f.input :tag_list, :label => "Surf Culture", :as => :check_boxes, :collection => ["Bodysurfing", "Shaping", "surf films", "contests", "night surfing", "big wave", "cold water surfing", "surf exploration", "surf gear", "travel", "performance", "style", "surfboard design", "surfboard technology", "world tour", "surf film"]
      f.input :tag_list, :label => "Health", :as => :check_boxes, :collection => ["yoga", "organic", "first aid", "cooking", "health food", "wellness", "fitness"]
      f.input :tag_list, :label => "Self Reliance", :as => :check_boxes, :collection => ["DIY", "gardening", "Camping", "fermentation/brewing", "fishing/hunting", "water harvesting", "solar", "maintenance and repair", "building", "how to", "craft", "handmade"]
      f.input :tag_list, :label => "Environment", :as => :check_boxes, :collection => ["recycling", "CO2", "pollution", "sustainability"]
      f.input :tag_list, :label => "Events", :as => :check_boxes, :collection => ["film premiere", "art show", "workshop", "contest", "film festival", "festival", "screening", "surfboard convention"]
      f.input :tag_list, :label => "Media", :as => :check_boxes, :collection => ["film", "digital", "tech", "DSLR", "GoPro", "post production", "shooting", "animation", "time lapse", "documentary", "magazine", "website", "zine", "Filmmaking", "Photography", "Film Trailer", "Super 8"]
      f.input :tag_list, :label => "Themes", :as => :check_boxes, :collection => ["Art", "Craft", "Food", "Home", "Apparel", "Art", "Books", "Outdoors", "Skate", "Snow", "Wood", "3D", "self reliance", "health", "media", "surf crafts", "surf culture", "business", "environment", "events", "self sufficient", "travel", "music", "rock climbing", "auto", "hipster", "holidays", "gifts", "women", "gear", "rant", "vintage", "culture", "non profit org", "comedy", "nature", "adventure", "community", "bicycle", "technology", "design", "artist"]
    end
    
    f.inputs 'Videos' do
      f.input :vimeo
      #f.input :youtube
    end
    f.inputs 'Images' do
      f.input :thumbnail, :hint => f.template.image_tag(f.object.thumbnail) unless f.object.thumbnail.blank?
      #f.input :poster, :input_html => { :readonly => true }, :hint => f.template.image_tag(f.object.poster) unless f.object.poster.blank?
    end
    f.has_many :credits do |c|
      # The delete checkbox breaks if it isn't at the top for some reason
      # https://github.com/gregbell/active_admin/issues/59
      unless c.object.id.nil?
        c.input :_destroy, :as => :boolean, :label => "delete"
      end
      c.input :credit_type
      c.input :name
      c.input :url
    end
    f.has_many :comments do |c|
      # The delete checkbox breaks if it isn't at the top for some reason
      # https://github.com/gregbell/active_admin/issues/59
      unless c.object.id.nil?
        c.input :_destroy, :as => :boolean, :label => "delete"
      end
      c.input :title
      c.input :comment, :input_html => {  :rows => 6 }
    end
    f.buttons
  end
  
  index :as => :grid, :columns => 4 do |show|
    div :class => "showGrid" do
      link_to(image_tag(show.thumbnail) + show.title, admin_show_path(show))
    end
  end
  
  show do
    #attributes_table :vimeo, :youtube
    attributes_table :title, :description, :vimeo, :youtube, :thumbnail, :show_category, :comments_closed, :staff_pick, :slug, :impressions, :average_rating
  end
end
