ActiveAdmin.register MakerContact do
  menu :label => "Maker Contacts", :parent => "Misc", :if => proc { can?(:read, MakerContact)}
  
  index do
    column :maker
    column :name
    column :email
    column :created_at
    default_actions
  end
  
  form do |f|
    f.inputs do
      f.input :maker
      f.input :name
      f.input :email
      f.input :message
    end
    f.buttons
  end
  
  controller do
    def create
      @maker_contact = MakerContact.new(params[:maker_contact])
      
      @maker_contact.ip = request.remote_ip
      
      respond_to do |format|
        if @maker_contact.save
          format.html { redirect_to admin_maker_contacts_path, notice: 'Contact was successfully created.' }
          format.json { render json: @maker_contact, status: :created, location: @maker_contact }
        else
          format.html { render action: "new" }
          format.json { render json: @maker_contact.errors, status: :unprocessable_entity }
        end
      end
    end
  end
end
