ActiveAdmin.register Order do
  menu :if => proc { can?(:read, Order)}
  
  index do
    column :order_id
    column :customer_billing_name
    column :order_financial_status
    column :order_date
    default_actions
  end
  
  form :html => { :enctype => "multipart/form-data" } do |f|
    f.inputs do
      f.input :order_date
      f.input :order_id, :input_html => { :readonly => "readonly" }
      f.input :order_number, :input_html => { :readonly => "readonly" }
      f.input :order_financial_status, :input_html => { :readonly => "readonly" }
      f.input :order_fulfillment_status, :input_html => { :readonly => "readonly" }
      f.input :order_note, :input_html => {  :rows => 3 }
    end
    
    f.inputs :name => "Customer Billing Info" do
      f.input :customer_email
      f.input :customer_billing_name
      f.input :customer_billing_phone
    end
    
    f.inputs :name => "Customer Shipping Info" do
      f.input :customer_shipping_name
      f.input :customer_shipping_email
      f.input :customer_shipping_phone
      f.input :customer_shipping_address1
      f.input :customer_shipping_address2
      f.input :customer_shipping_city
      f.input :customer_shipping_province
      f.input :customer_shipping_zip
      f.input :customer_shipping_country, :as => :string
    end
      
    f.has_many :order_items do |item|
      item.input :maker_id
      item.input :item_qty
      item.input :item_name
      item.input :item_price
      item.input :item_vendor
      item.input :item_sku, :input_html => { :readonly => "readonly" }
      item.input :item_variant_id, :input_html => { :readonly => "readonly" }
      item.input :item_weight
      item.input :item_fulfillment_status, :input_html => { :readonly => "readonly" }
    end
    f.buttons
  end
end
