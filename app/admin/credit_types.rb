ActiveAdmin.register CreditType do
  menu :label => "Credit Types", :parent => "Shows", :if => proc { can?(:read, CreditType)}
end
