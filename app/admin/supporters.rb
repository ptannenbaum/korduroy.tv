ActiveAdmin.register Supporter do
  menu :parent => "Supporters", :if => proc { can?(:read, Supporter)}
  
  index do
    column :display_order
    column :logo do |supporter|
      image_tag(supporter.logo)
    end
    column :name
    column :website
    column :email do |supporter|
      mail_to supporter.email, supporter.email
    end
    column :ad do |supporter|
      image_tag(supporter.ad)
    end
    default_actions
  end
  
  form :html => { :enctype => "multipart/form-data" } do |f|
    f.inputs do
      f.input :supporter_category
      f.input :display_order
      f.input :name
      f.input :address
      f.input :city
      f.input :state
      f.input :zip_code
      f.input :telephone
      f.input :website
      f.input :email
      if f.object.new_record? || f.object.logo.blank?
        f.input :logo, :as => :file
      else
        f.input :logo, :as => :file, :hint => f.template.image_tag(f.object.logo.url)
        f.input :logo_cache, :as => :hidden
      end
      if f.object.new_record? || f.object.ad.blank?
        f.input :ad, :as => :file
      else
        f.input :ad, :as => :file, :hint => f.template.image_tag(f.object.ad.url)
        f.input :ad_cache, :as => :hidden
      end
      f.input :description
    end
    f.buttons
  end
end
