ActiveAdmin.register Project do
  menu :parent => "Misc", :if => proc { can?(:read, Project)}
  
  index do
    column :title
    column :start_date
    column :end_date
    default_actions
  end
  
  #plupload image uploader for content images
  #sidebar :content_images, :only => :edit, :partial => "content_images"
  
  form :html => { :enctype => "multipart/form-data" } do |f|
    f.inputs do
      if f.object.new_record?
        f.input :title, :input_html => { :class => 'slug_input' }
      else
        f.input :title
      end
      f.input :slug, :input_html => { :class => 'slug' }
      
      f.input :start_date
      f.input :end_date
      f.input :body
    end
    f.buttons
  end
end
