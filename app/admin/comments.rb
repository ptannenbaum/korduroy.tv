ActiveAdmin.register Comment, :as => "User Comments" do
  
  filter :commentable_type, :as => :check_boxes, :collection => ["Blog", "Show", "Poll"]
  filter :user
  
  index do
    column :user
    column "Content Type", :commentable_type
    column "Title", :commentable_type do |c|
      case c.commentable_type
        when "Blog"
          @content = Blog.where(:id => c.commentable_id).first
        when "Show"
          @content = Show.where(:id => c.commentable_id).first
        when "Poll"
          @content = Poll.where(:id => c.commentable_id).first
      end
      if @content.blank?
        "-- No Content Found --"
      else
        if c.commentable_type == "Poll"
          link_to @content.question, "/admin/#{c.commentable_type.downcase.pluralize}/#{c.commentable_id}/edit"
        else
          link_to @content.title, "/admin/#{c.commentable_type.downcase.pluralize}/#{c.commentable_id}/edit"
        end
      end
    end
    column :comment
    column :created_at
    default_actions
  end
end