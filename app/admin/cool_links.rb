ActiveAdmin.register CoolLink do
  menu :label => "Links", :parent => "Cool Links", :if => proc { can?(:read, CoolLink)}
  
  index do
    column :name
    column :url
    column :cool_category_id
    default_actions
  end
  
  form :html => { :enctype => "multipart/form-data" } do |f|
    f.inputs do
      f.input :cool_category
      f.input :name
      f.input :url
    end
    f.buttons
  end
end
