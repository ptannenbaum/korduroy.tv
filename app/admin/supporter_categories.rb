ActiveAdmin.register SupporterCategory do
  menu :parent => "Supporters", :label => "Categories", :if => proc { can?(:read, SupporterCategory)}
  
  index do
    column :display_order
    column :name
    column :slug
    default_actions
  end
  
  form :html => { :enctype => "multipart/form-data" } do |f|
    f.inputs do
      f.input :display_order
      if f.object.new_record?
        f.input :name, :input_html => { :class => 'slug_input' }
      else
        f.input :name
      end
      f.input :slug, :input_html => { :class => 'slug' }
    end
    f.buttons
  end
end
