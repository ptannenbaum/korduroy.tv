ActiveAdmin.register Poll do
  menu :label => "Questions", :parent => "The List", :if => proc { can?(:read, Poll)}
  
  index do
    column :question
    column :poll_category
    column :active
    default_actions
  end
  
  form :html => { :enctype => "multipart/form-data" } do |f|
    f.inputs do
      f.input :poll_category
      f.input :question
      f.input :active
    end
    f.has_many :answers do |a|
      # The delete checkbox breaks if it isn't at the top for some reason
      # https://github.com/gregbell/active_admin/issues/59
      unless a.object.id.nil?
        a.input :_destroy, :as => :boolean, :label => "delete"
      end
      a.input :name
      a.input :url
      a.input :position
      if a.object.new_record? || a.object.image.blank?
        a.input :image, :as => :file
      else
        a.input :image, :as => :file, :hint => a.template.image_tag(a.object.image.small.url)
        a.input :image_cache, :as => :hidden
      end
    end
    f.buttons
  end
end
