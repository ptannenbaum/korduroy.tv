ActiveAdmin.register ShowCategory do
  menu :label => "Categories", :parent => "Shows", :if => proc { can?(:read, ShowCategory)}
  
  form :html => { :enctype => "multipart/form-data" } do |f|
    f.inputs do
      f.input :display_order
      if f.object.new_record?
        f.input :title, :input_html => { :class => 'slug_input' }
      else
        f.input :title
      end
      f.input :slug, :input_html => { :class => 'slug' }
      f.input :description
    end
    f.inputs 'Images' do
      if f.object.new_record? || f.object.icon.blank?
        f.input :icon, :as => :file
        f.input :banner, :as => :file
      else
        f.input :icon, :as => :file, :hint => f.template.image_tag(f.object.icon.url)
        f.input :banner, :as => :file
      end
    end
    f.buttons
  end
  
  index do
    column :display_order
    column :icon do |show|
      image_tag(show.icon) if show.icon?
    end
    column :title
    column :slug
    default_actions
  end
end
