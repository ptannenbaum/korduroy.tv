ActiveAdmin::Dashboards.build do

  # Define your dashboard sections here. Each block will be
  # rendered on the dashboard in the context of the view. So just
  # return the content which you would like to display.
  
  # == Simple Dashboard Section
  # Here is an example of a simple dashboard section
  #
  #   section "Recent Posts" do
  #     ul do
  #       Post.recent(5).collect do |post|
  #         li link_to(post.title, admin_post_path(post))
  #       end
  #     end
  #   end
  
  section "Recent Comments" do
    table_for Comment.reorder("created_at desc").limit(30), :class => "recent-comments-table" do
      column :user
      column "Content", :commentable_type
      column "Title", :commentable_type do |c|
        case c.commentable_type
          when "Blog"
            @content = Blog.where(:id => c.commentable_id).first
          when "Show"
            @content = Show.where(:id => c.commentable_id).first
          when "Poll"
            @content = Poll.where(:id => c.commentable_id).first
        end
        if @content.blank?
          "-- No Content Found --"
        else
          if c.commentable_type == "Poll"
            link_to @content.question, "/admin/#{c.commentable_type.downcase.pluralize}/#{c.commentable_id}/edit"
          else
            link_to @content.title, "/admin/#{c.commentable_type.downcase.pluralize}/#{c.commentable_id}/edit"
          end
        end
      end
      column :comment
      column("Date Posted") { |comment| l(comment.created_at, :format => :poll)}
      column "Actions" do |comment|
        ul do
          li link_to "View", admin_user_comment_path(comment)
          li link_to "Edit", edit_admin_user_comment_path(comment)
          li link_to "Delete", admin_user_comment_path(comment), :method => :delete, :confirm => "Are you sure you want to delete this item?"
        end
      end
    end
    div do
      link_to("View all comments", admin_user_comments_path)
    end
  end
  
  # == Render Partial Section
  # The block is rendered within the context of the view, so you can
  # easily render a partial rather than build content in ruby.
  #
  #   section "Recent Posts" do
  #     div do
  #       render 'recent_posts' # => this will render /app/views/admin/dashboard/_recent_posts.html.erb
  #     end
  #   end
  
  # == Section Ordering
  # The dashboard sections are ordered by a given priority from top left to
  # bottom right. The default priority is 10. By giving a section numerically lower
  # priority it will be sorted higher. For example:
  #
  #   section "Recent Posts", :priority => 10
  #   section "Recent User", :priority => 1
  #
  # Will render the "Recent Users" then the "Recent Posts" sections on the dashboard.

end
