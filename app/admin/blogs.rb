ActiveAdmin.register Blog do
  menu :label => "Posts", :parent => "Blogs", :if => proc { can?(:read, Blog)}
  
  scope :all
  scope :ktv_blog_posts, :default => true
  scope :maker_blog_posts
  
  controller do
    def create
      @blog = Blog.new(params[:blog])
      
      @blog.user = current_user
      @blog.comments.each { |comment| comment.user = current_user if comment.new_record? }
      
      respond_to do |format|
        if @blog.save
          format.html { redirect_to admin_blogs_path, notice: 'Blog was successfully created.' }
          format.json { render json: @blog, status: :created, location: @blog }
        else
          format.html { render action: "new" }
          format.json { render json: @blog.errors, status: :unprocessable_entity }
        end
      end
    end
    
    def update
      @blog = Blog.find(params[:id])
      @blog.attributes = params[:blog]
      @blog.comments.each { |comment| comment.user = current_user if comment.new_record? }
      
      if @blog.user.nil?
        @blog.user = current_user
      end
      
      respond_to do |format|
        if @blog.save
          format.html { redirect_to admin_blogs_path, notice: 'Blog was successfully updated.' }
          format.json { head :ok }
        else
          format.html { render action: "edit" }
          format.json { render json: @blog.errors, status: :unprocessable_entity }
        end
      end
    end
  end
  
  index do
    column :post_image do |blog|
      image_tag(blog.post_image.small, :class => "postImage") unless blog.post_image.blank?
    end
    column :title
    column "Category", :blog_category
    column :user
    column :visible
    column :created_at
    default_actions
  end
  
  #show do
  #  h3 blog.title
  #  div do
  #    sanitize blog.content
  #  end
  #end
  
  #plupload image uploader for content images
  sidebar :content_images, :only => :edit, :partial => "content_images"
  
  form :html => { :enctype => "multipart/form-data" } do |f|
    f.inputs do
      f.input :blog_category
      if f.object.new_record?
        f.input :title, :input_html => { :class => 'slug_input' }
      else
        f.input :title
      end
      f.input :slug, :input_html => { :class => 'slug' }
      if f.object.new_record? || f.object.post_image.blank?
        f.input :post_image, :as => :file
      else
        f.input :post_image, :as => :file, :hint => f.template.image_tag(f.object.post_image.url(:large), :class => "postImage")
        f.input :post_image_cache, :as => :hidden
      end
      f.input :content, :hint => "Media and images should have a max width of 650px"
      #f.input :tag_list, :as => :check_boxes, :collection => ["Apparel", "Art", "Bodysurfing", "Books", "Camping", "Contests", "Craft", "Environment", "Events", "Filmmaking", "Finless", "Fishes", "Food", "Garden", "Health", "Heshians", "Hippies", "Home", "Jocks", "Longboards", "Lumberjacks", "Outdoors", "Organizations", "Photography", "Preppies", "Shaping", "Shortboards", "Skate", "Snow", "Technology", "Travel", "Trailers", "Wood"]
      f.input :visible
      f.input :comments_closed
      f.input :created_at, :ampm => true
    end
    # tags
    f.inputs :name => "Tags" do
      f.input :tag_list, :label => "People", :as => :check_boxes, :collection => ["Kelly Slater", "Ryan Burch", "Ryan Tatar", "Cyrus Sutton", "Ryan Lovelace", "Danny Hess", "Richard Kenvin", "Dane Reynolds", "Derek Dunfee", "Tyler Warren", "Neal Purchase", "Daniel Thompson", "Dave Allee", "Wegener", "Adrian Knott", "Derek Hynd", "Ed Sloane", "Steve Pendarvis", "Jeremy Rumas", "Jack Coleman"]
      f.input :tag_list, :label => "Places", :as => :check_boxes, :collection => ["Canada", "Australia", "Japan", "Europe", "Spain", "UK", "Mexico", "Indo", "New York", "Brazil", "West Oz", "Central America", "South America", "East coast US", "South Pacific", "So Cal", "Nor Cal", "beach break", "point break", "San Diego", "Hawaii", "New Zealand", "Africa", "Central California"]
      f.input :tag_list, :label => "Surfcrafts", :as => :check_boxes, :collection => ["handplane", "alaia", "paipo", "asymmetrics", "kneeboarding", "prone surfing", "2+1", "noseriding", "thruster", "quad", "single fin", "twin fin", "EPS", "Finless", "Longboards", "Shortboards", "hull", "lord board", "bodyboard", "Pendoflex"]
      f.input :tag_list, :label => "Business", :as => :check_boxes, :collection => ["time management", "industry"]
      f.input :tag_list, :label => "Surf Culture", :as => :check_boxes, :collection => ["Bodysurfing", "Shaping", "surf films", "contests", "night surfing", "big wave", "cold water surfing", "surf exploration", "surf gear", "travel", "performance", "style", "surfboard design", "surfboard technology", "world tour", "surf film"]
      f.input :tag_list, :label => "Health", :as => :check_boxes, :collection => ["yoga", "organic", "first aid", "cooking", "health food", "wellness", "fitness"]
      f.input :tag_list, :label => "Self Reliance", :as => :check_boxes, :collection => ["DIY", "gardening", "Camping", "fermentation/brewing", "fishing/hunting", "water harvesting", "solar", "maintenance and repair", "building", "how to", "craft", "handmade"]
      f.input :tag_list, :label => "Environment", :as => :check_boxes, :collection => ["recycling", "CO2", "pollution", "sustainability"]
      f.input :tag_list, :label => "Events", :as => :check_boxes, :collection => ["film premiere", "art show", "workshop", "contest", "film festival", "festival", "screening", "surfboard convention"]
      f.input :tag_list, :label => "Media", :as => :check_boxes, :collection => ["film", "digital", "tech", "DSLR", "GoPro", "post production", "shooting", "animation", "time lapse", "documentary", "magazine", "website", "zine", "Filmmaking", "Photography", "Film Trailer", "Super 8"]
      f.input :tag_list, :label => "Themes", :as => :check_boxes, :collection => ["Art", "Craft", "Food", "Home", "Apparel", "Art", "Books", "Outdoors", "Skate", "Snow", "Wood", "3D", "self reliance", "health", "media", "surf crafts", "surf culture", "business", "environment", "events", "self sufficient", "travel", "music", "rock climbing", "auto", "hipster", "holidays", "gifts", "women", "gear", "rant", "vintage", "culture", "non profit org", "comedy", "nature", "adventure", "community", "bicycle", "technology", "design", "artist"]
    end
    # comments
    f.has_many :comments do |c|
      # The delete checkbox breaks if it isn't at the top for some reason
      # https://github.com/gregbell/active_admin/issues/59
      unless c.object.id.nil?
        c.input :_destroy, :as => :boolean, :label => "delete"
      end
      c.input :title
      c.input :comment, :input_html => {  :rows => 6 }
      c.input :user
    end
    # action buttons
    f.buttons
  end
end
