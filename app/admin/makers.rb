ActiveAdmin.register Maker do
  menu :label => "Profiles", :parent => "Makers", :if => proc { can?(:read, Maker)}
  
  controller do
    def create
      @maker = Maker.new(params[:maker])
      
      @maker.crew     = params[:maker][:crew]
      

      @maker.maker_videos.each do |video|
        if video.thumbnail.blank? && !video.vimeo.blank?
          @v = Vimeo::Simple::Video.info(video.vimeo)
          video.thumbnail = @v[0]['thumbnail_medium']
        end
        
        if video.thumbnail.blank? && !video.youtube.blank?
          video.thumbnail = "http://img.youtube.com/vi/" + video.youtube + "/0.jpg"
        end
      end

      
      respond_to do |format|
        if @maker.save
          format.html { redirect_to admin_makers_path, notice: 'Maker was successfully created.' }
          format.json { render json: @maker, status: :created, location: @maker }
        else
          format.html { render action: "new" }
          format.json { render json: @maker.errors, status: :unprocessable_entity }
        end
      end
    end
    
    def update
      @maker = Maker.find(params[:id])
      
      @maker.attributes = params[:maker]
      
      # we are explicitly setting these because they should only be set
      # by admins
      @maker.crew     = params[:maker][:crew]
      
      @maker.maker_videos.each do |video|
        if video.thumbnail.blank? && !video.vimeo.blank?
          @v = Vimeo::Simple::Video.info(video.vimeo)
          video.thumbnail = @v[0]['thumbnail_medium']
        end
        
        if video.thumbnail.blank? && !video.youtube.blank?
          video.thumbnail = "http://img.youtube.com/vi/" + video.youtube + "/0.jpg"
        end
      end
      
      respond_to do |format|
        if @maker.save
          format.html { redirect_to admin_makers_path, notice: 'Maker was successfully updated.' }
          format.json { head :ok }
        else
          format.html { render action: "edit" }
          format.json { render json: @maker.errors, status: :unprocessable_entity }
        end
      end
    end
  end
  
  index do
    column :user do |maker|
      unless maker.user.blank?
        if maker.user.avatar?
          image_tag(maker.user.avatar.small)
        else
          image_tag("/assets/avatar_small.png")
        end
      end
    end
    column :name
    column :title
    column :maker_category
    column :storefront
    column :visible
    column :crew
    default_actions
  end
  
  form :html => { :enctype => "multipart/form-data" } do |f|
    f.inputs do
      if f.object.new_record?
        f.input :name, :input_html => { :class => 'slug_input' }
      else
        f.input :name
      end
      f.input :slug, :input_html => { :class => 'slug' }
      f.input :title
      f.input :maker_category
      f.input :bio
      f.input :details
      if f.object.new_record? || f.object.bio_image.blank?
        f.input :bio_image, :as => :file
      else
        f.input :bio_image, :as => :file, :hint => f.template.image_tag(f.object.bio_image.url(:large))
        f.input :bio_image_cache, :as => :hidden
      end
      f.input :visible
      f.input :storefront
      f.input :collection_id, :label => "Shopify Custom Collection ID"
      f.input :crew
      f.input :external_blog_feed
      f.input :vimeo, :label => "URL of your Vimeo profile"
      f.input :youtube, :label => "URL of your YouTube profile"
      f.input :other_url, :label => "URL fo your website"
      f.input :allow_contact_form
      f.has_many :maker_photos do |photo|
        # The delete checkbox breaks if it isn't at the top for some reason
        # https://github.com/gregbell/active_admin/issues/59
        unless photo.object.id.nil?
          photo.input :_destroy, :as => :boolean, :label => "delete"
        end
        photo.input :title
        photo.input :description, :input_html => { :rows => 1 }
        photo.input :visible
        photo.input :comments_closed
        if photo.object.new_record? || photo.object.photo.blank?
          photo.input :photo, :as => :file
        else
          photo.input :photo, :as => :file, :hint => photo.template.image_tag(photo.object.photo.url(:small))
          photo.input :photo_cache, :as => :hidden
        end
      end
      f.has_many :maker_videos do |video|
        # The delete checkbox breaks if it isn't at the top for some reason
        # https://github.com/gregbell/active_admin/issues/59
        unless video.object.id.nil?
          video.input :_destroy, :as => :boolean, :label => "delete"
        end
        video.input :title
        video.input :description, :input_html => { :rows => 1 }
        video.input :visible
        video.input :comments_closed
        video.input :vimeo, :label => "Vimeo Video ID"
        video.input :youtube, :label => "YouTube Video ID"
        video.input :thumbnail#, :hint => video.template.image_tag(video.object.thumbnail, :class => "thumb video")
      end
    end
    f.buttons
  end
end