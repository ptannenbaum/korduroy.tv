ActiveAdmin.register CampaignBanner do
  menu :label => "Campaign Banner", :if => proc { can?(:read, CampaignBanner)}

  controller do
    def create
      @show_quiver = CampaignBanner.new(params[:campaign_banner])

      respond_to do |format|
        if @show_quiver.save
          format.html { redirect_to admin_campaign_banners_path, notice: 'Banner was successfully created.' }
          format.json { render json: @show_quiver, status: :created, location: @show_quiver }
        else
          format.html { render action: "new" }
          format.json { render json: @show_quiver.errors, status: :unprocessable_entity }
        end
      end
    end

    def update
      @show_quiver = CampaignBanner.find(params[:id])
      @show_quiver.attributes = params[:campaign_banner]


      respond_to do |format|
        if @show_quiver.save
          format.html { redirect_to admin_campaign_banners_path, notice: 'Banner was successfully updated.' }
          format.json { head :ok }
        else
          format.html { render action: "edit" }
          format.json { render json: @show_quiver.errors, status: :unprocessable_entity }
        end
      end
    end
  end

  index do
    column :post_image do |show_quiver|
      image_tag(show_quiver.post_image, :class => "postImage") unless show_quiver.post_image.blank?
    end
    column :link

    column :show_home
    column :created_at
    default_actions
  end



  form :html => { :enctype => "multipart/form-data" } do |f|

    f.inputs do

      if f.object.new_record? || f.object.post_image.blank?
        f.input :post_image, :as => :file, :label => "Image of size 260x56"
      else
        f.input :post_image, :as => :file, :label => "Image of size 260x56", :hint => f.template.image_tag(f.object.post_image.url(:large), :class => "postImage")
        f.input :post_image_cache, :as => :hidden
      end


      f.input :show_home
      f.input :link


    end

    f.buttons
  end
end
