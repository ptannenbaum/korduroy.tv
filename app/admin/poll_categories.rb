ActiveAdmin.register PollCategory do
  menu :label => "Categories", :parent => "The List", :if => proc { can?(:read, Poll)}

   index do
     column :name
     column :slug
     default_actions
   end

   form :html => { :enctype => "multipart/form-data" } do |f|
     f.inputs do
       if f.object.new_record?
         f.input :name, :input_html => { :class => 'slug_input' }
       else
         f.input :name
       end
       f.input :slug, :input_html => { :class => 'slug' }
     end
     f.buttons
   end
end
