class CoolController < ApplicationController
  before_filter :set_nav_states
  
  def index
    @categories = CoolCategory.all
    @links = CoolLink.all
  end

  private

    def set_nav_states
      @ktvCoolActive = "active"
    end
end
