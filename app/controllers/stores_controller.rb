class StoresController < ApplicationController
  before_filter :set_nav_states, :get_shopify_data
  
  def index
    @hero = @featured.first
    @featured = @featured[1..6]
    #@products = Kaminari.paginate_array(@products).page(params[:page]).per(40)
  end
  
  def show
    @product = ShopifyAPI::Product.find(params[:id])
    tagArray = Array[]
    unless @product.tags.blank?
      tags = @product.tags.split(",")
      tags.each do |t|
        tagArray.push t.strip
      end
    end
    @tags = tagArray
    
    if @product.variants.size > 1
      v1 = Array[]
      v2 = Array[]
      v3 = Array[]
      @product.variants.each do |v|
        v1.push v.option1
        v2.push v.option2
        v3.push v.option3
      end
      @v1 = v1.uniq
      @v2 = v2.uniq
      @v3 = v3.uniq
    end
    prod_array = Array[]
    @products.each do |p|
      unless p.id == @product.id
        unless @product.tags.blank?
          tags.each do |t|
            unless p.tags.blank?
              if p.tags.downcase.include? t.to_s.strip.downcase
                prod_array.push p
              end
            end
          end
        end
      end
    end
    @related_products = prod_array.uniq.sort_by{ rand }.slice(0..4)
    @related_maker = Maker.get_related_maker(@product.vendor).first
    @related_shows = Show.tagged_with(@tags, :any => true).limit(5)
    @related_blogs = Blog.tagged_with(@tags, :any => true).limit(5)
    @related_items = @related_products + @related_shows + @related_blogs
  end
  
  def cart
  end
  
  private

    def set_nav_states
      @ktvStoreActive = "active"
    end
  
    def get_shopify_data
      store_yaml =  Rails.cache.fetch('store', :expires_in => 30.minutes) do
        @products = ShopifyAPI::Product.find(:all, :params => {:collection_id => 4503832, :published_status => "published", :limit => 250 }).to_yaml
      end
      collects_yaml = Rails.cache.fetch('collects', :expires_in => 30.minutes) do
        @collects = ShopifyAPI::Collect.find(:all, :params => {:collection_id => 4503832, :limit => 250}).to_yaml
      end
      ShopifyAPI::Base.format = :xml
      
      @products = YAML::load(store_yaml)
      @collects = YAML::load(collects_yaml)
      
      @collects = @collects.uniq.sort! { |a,b| a.position <=> b.position }
      unless @products.blank?
        catArray = Array[]
        tagArray = Array[]
        featArray = Array.new(7)
       @products.each do |p|
          unless p.product_type.blank?
            catArray.push p.product_type.downcase.strip
          end
          unless p.tags.blank?
            tags = p.tags.split(",")
            tags.each do |t|
              tagArray.push t.downcase.strip
            end
          end
          # get display order for featured products
          @collects.each_with_index do |col, i|
            if col.product_id == p.id
              featArray.fill(p, i, 1)
            end
          end
          #for i in 0..6
          #  if @collects[i].product_id == p.id
          #    featArray.fill(p, @collects[i].position-1, 1)
          #  end
          #end
       end
        @categories = catArray.uniq.sort!
        if tagArray.size > 0
          freq = tagArray.inject(Hash.new(0)) { |h,v| h[v] += 1; h }
          @tags = tagArray.sort_by { |v| freq[v] }.uniq.last(10).reverse
        else
          @tags = tagArray
        end
        @featured = featArray.compact.slice(0..6)
      end
    end
    
end
