class RegistrationsController < Devise::RegistrationsController
  
  protected
    
    def after_update_path_for(resource)
      status_path(resource)
    end
    
    def after_sign_up_path_for(resource)
      status_path
    end
    
    def after_sending_reset_password_instructions_path_for(resource_name)
      status_path
    end
end
