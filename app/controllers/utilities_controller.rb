class UtilitiesController < ApplicationController
  def load_external_feeds
    makers = Maker.makers_with_external_blogs
    feeds = {}
    makers.each { |maker| feeds[maker.external_blog_feed] = maker }
    rss_feeds = Feedzirra::Feed.fetch_and_parse(feeds.keys)
    makers.each do |maker|
      next if maker.external_blog_feed == nil or
        rss_feeds[maker.external_blog_feed] == 0 or
        rss_feeds[maker.external_blog_feed] == nil or
        rss_feeds[maker.external_blog_feed] == 404
      rss_feeds[maker.external_blog_feed].entries.each do |entry|
        blog = Blog.new(
          :url              => entry.url,
          #:tag_list         => entry.categories.join(', '),
          :visible          => true,
          :created_at       => entry.published,
          :comments_closed  => false
        )
        blog.user_id = maker.user.id
        if entry.content.blank?
          blog.content = entry.summary
        else
          blog.content = entry.content
        end
        if entry.title.blank?
          blog.title  = "#{maker.name} POST #{Blog.where(:user_id => blog.user_id).count + 1}"
          blog.slug   = blog.title.downcase.strip.gsub(' ', '-').gsub(/[^\w-]/, '')
        else
          blog.title  = entry.title
          blog.slug   = entry.title.downcase.strip.gsub(' ', '-').gsub(/[^\w-]/, '')
        end
        if blog.save
          puts 'Saved'
        else
          puts blog.errors.messages
        end
      end
    end
    render :text => "Success"
  end
end
