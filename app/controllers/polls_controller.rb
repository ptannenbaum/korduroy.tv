class PollsController < ApplicationController
  before_filter :load_categories, :set_nav_states
  before_filter :authenticate_user!, :only => 'vote'
  
  def index
    @polls = Poll.order("id DESC")
  end
  
  def category
    @category = PollCategory.find_by_slug!(params[:slug])
    @polls = @category.polls.order("id DESC")
  end
  
  def show
    @poll = Poll.find(params[:id])
    @comments = @poll.comments.all
    @current_user = current_user
  end
  
  def vote
    @poll = Poll.find(params[:id])
    PollResponse.save_votes(current_user, @poll, params[:votes])
    render :text => @poll.tally_responses.to_json
  end
  
  def comment
    @poll = Poll.find(params[:id])
    @comment = @poll.comments.create(params[:comment])
    @comment.user = current_user
    respond_to do |format|
      if @comment.save
        format.html { redirect_to url_for(:id => @poll), notice: 'Thanks for your comments.' }
        format.json { render json: @comment, status: :created, location: @poll }
      else
        format.html { render action: "new" }
        format.json { render json: @comment.errors, status: :unprocessable_entity }
      end
    end
  end

private
  def load_categories
    @categories = PollCategory.all
  end

  def set_nav_states
    @ktvListActive = "active"
  end
end
