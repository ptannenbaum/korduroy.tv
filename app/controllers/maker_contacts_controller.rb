class MakerContactsController < ApplicationController
  def new
    @maker = Maker.find_by_slug!(params[:slug])
    @contactActive = "active"
    
    @maker_contact = MakerContact.new
    
    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @maker_contact }
    end
  end

  # POST /admin/polls
  # POST /admin/polls.json
  def create
    @maker_contact = MakerContact.new(params[:maker_contact])
    @maker_contact.ip = request.remote_ip
    @maker_contact.maker = Maker.find_by_slug(params[:slug])
    
    respond_to do |format|
      if @maker_contact.save
        ContactMailer.maker_contact(@maker_contact).deliver
        format.html { redirect_to url_for(:action => 'thanks'), notice: 'Your message has been delivered.' }
        format.json { render json: @maker_contact, status: :created, location: @maker_contact }
      else
        format.html { render action: "new" }
        format.json { render json: @maker_contact.errors, status: :unprocessable_entity }
      end
    end
  end
  
  def thanks
    @maker = Maker.find_by_slug!(params[:slug])
    @contactActive = "active"
  end
end
