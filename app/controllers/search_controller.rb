class SearchController < ApplicationController
  def results
    query = CGI::escape(params[:q])
    page  = params[:page].blank? ? 1 : CGI::escape(params[:page]).to_i
    @search = Hash.from_xml(open("http://beta.korduroy.tv:8080/searchblox/servlet/SearchServlet?col=4&query=#{query}&page=#{page}").read)
    @results        = @search['searchdoc']['results']['result']
    @current_page   = @search['searchdoc']['results']['currentpage'].to_i
    @last_page      = @search['searchdoc']['results']['lastpage'].to_i
    @hits           = @search['searchdoc']['results']['hits'].to_i
    @previous_page  = @current_page - 1
    @next_page      = @current_page + 1
  end
end