class ApplicationController < ActionController::Base
  protect_from_forgery
  
  ShopifyAPI::Base.site = "https://3fd9effe5aef663e58dfd78a918e3ef2:741f4006631fd33228e87ef4fd18d69b@korduroy.myshopify.com/admin"
  
  layout :layout_by_resource
  
  helper_method :yt_client
  
  def yt_client
    @yt_client ||= YouTubeIt::Client.new(:username => YouTubeITConfig.username , :password => YouTubeITConfig.password , :dev_key => YouTubeITConfig.dev_key)
  end
  
  # Had to add this in so that the user would be redirected
  # to the correct url after sign in.
  def after_sign_in_path_for(resource_or_scope)
    stored_location_for(resource_or_scope) || status_path
  end
  
  def render_success
    render :file => "#{Rails.root}/public/200.html", :status => 200
  end
  
  protected
    def authenticate_admin!
      authenticate_user!
      raise CanCan::AccessDenied unless current_user.admin?
    end

    def layout_by_resource
      if devise_controller?
        "plain"
      else
        "application"
      end
    end

  def authenticate_maker!
    authenticate_user!
    raise CanCan::AccessDenied unless current_user.is_maker?
  end

end
