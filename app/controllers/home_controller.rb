class HomeController < ApplicationController
  def index
    @ktv_posts = Blog.ktv_blog_posts.limit(5)

    @makers = Maker.where(:visible => true)
    @maker_posts = Blog.maker_blog_posts.limit(4)

    products_yaml =  Rails.cache.fetch('products', :expires_in => 30.minutes) do
      @products = ShopifyAPI::Product.find(:all, :params => {:collection_id => 4503832, :published_status => "published"}).to_yaml
    end

    @products = YAML::load(products_yaml)
    @featured_product = @products.first
    @featured_products = @products.sort_by{ rand }.slice(1..12)

    @polls = Poll.order("id DESC").limit(8).includes(:answers)
    @poll_home = true
    
    # scroller items
    @scroll_ktv_blog1 = @ktv_posts.first
    @scroll_ktv_blog2 = @ktv_posts.second
    @scroll_ktv_blog3 = @ktv_posts.third
    @scroll_shows = Show.limit(6).order('created_at DESC')
    @show1 = @scroll_shows[0]
    @show2 = @scroll_shows[1]
    @show3 = @scroll_shows[2]
    @scroll_maker_blog = @maker_posts.first
    @scroll_products = @products.sort_by{ rand }.slice(1..2)
  end

  def add_newsletter
    gb = Gibbon.new("837c3ca82822577ecf3f48bf67bcd9e0-us1")
    gb.throws_exceptions = false
    list = gb.lists({:filters => {:list_name => 'Korduroy.tv List'}})
    tmp = list.collect { |k, v| v }
    val =  tmp[1][0].collect{|k,v| v}
    resp = nil
    flag = true
    if(params[:newsletter] != "")
      resp = gb.list_subscribe({:id => val[0], :email_address => params[:newsletter].to_s})
    end

    if(resp == true)
      head :ok, :message => "Thank you for subscribing newsletter."
    else
      head :ok, :message => "You have already subscribed."
    end
  end
  
  def status
    render :layout => "plain"
  end
end
