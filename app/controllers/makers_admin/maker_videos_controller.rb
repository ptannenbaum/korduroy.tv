class MakersAdmin::MakerVideosController < ApplicationController
  before_filter :authenticate_user!, :set_nav_states
  load_and_authorize_resource
  layout 'application'
  
  def new
    @maker = current_user.maker
  end

  def edit
    @maker = current_user.maker
  end

  def create
    @maker_video.maker = current_user.maker
    
    # get thumbnail from vimeo or youtube
    if !@maker_video.vimeo.blank?
      @v = Vimeo::Simple::Video.info(@maker_video.vimeo)
      @maker_video.thumbnail = @v[0]['thumbnail_medium']
    end
    if !@maker_video.youtube.blank?
      @maker_video.thumbnail = "http://img.youtube.com/vi/" + @maker_video.youtube + "/0.jpg"
    end
    
    respond_to do |format|
      if @maker_video.save
        format.html { redirect_to "/makers/#{@maker_video.maker.slug}/gallery", notice: 'Video was successfully added.' }
        format.json { render json: @maker_video, status: :created, location: @maker_video }
      else
        format.html { render action: "new" }
        format.json { render json: @maker_video.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    respond_to do |format|
      if @maker_video.update_attributes(params[:maker_video])
        format.html { redirect_to "/makers/#{@maker_video.maker.slug}/gallery", notice: 'Video was successfully updated.' }
        format.json { head :ok }
      else
        format.html { render action: "edit" }
        format.json { render json: @maker_video.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @maker_video.destroy

    respond_to do |format|
      format.html { redirect_to "/makers/#{@maker_video.maker.slug}/gallery" }
      format.json { head :ok }
    end
  end
  
  private
     def set_nav_states
       @galleryActive = "active"
     end
end
