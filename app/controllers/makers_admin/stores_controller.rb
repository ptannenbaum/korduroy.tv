class MakersAdmin::StoresController < ApplicationController
  before_filter :authenticate_maker!, :set_nav_states, :get_product_options
  #load_and_authorize_resource
  layout 'application'
  
  def index
    maker = current_user.maker
    redirect_to "/makers/#{maker.slug}/store"
  end
  
  def new
    @maker = current_user.maker
  end
  
  def new_product
    maker = current_user.maker
    product = ShopifyAPI::Product.new
    product.collection_id = params[:collection_id]
    product.title = params[:title]
    product.product_type = params[:product_type]
    product.body_html = params[:body_html]
    product.tags = params[:tags]
    product.vendor = params[:vendor]
    
    # hackey way of avoiding 'nil name' error
    # one option
    options = [{"name" => params[:option_name_1]}]
    # two options
    unless params[:option_name_2].blank?
      options = [{"name" => params[:option_name_1]},{"name" => params[:option_name_2]}]
    end
    # three options
    unless params[:option_name_3].blank?
      options = [{"name" => params[:option_name_1]},{"name" => params[:option_name_2]},{"name" => params[:option_name_3]}] 
    end
    product.options = options
    
    variant = ShopifyAPI::Variant.new
    variant.option1 = params[:option_value_1]
    variant.option2 = params[:option_value_2]
    variant.option3 = params[:option_value_3]
    variant.price = params[:price]
    variant.weight = params[:weight]
    variant.sku = maker.slug + "-" + params[:sku]
    if !params[:track_inventory].nil?
      variant.inventory_management = "shopify"
      variant.inventory_quantity = params[:inventory_quantity]
    else
      variant.inventory_quantity = "1"
    end
    product.variants = variant
    
    respond_to do |format|
      if product.save
        # add product to maker's collection
        collect = ShopifyAPI::Collect.new
        collect.collection_id = params[:collection_id]
        collect.product_id = product.id
        collect.save
        current_user = maker.user
        
        product_image = ProductImage.new(params[:product_image])
        product_image.product_id = product.id
        product_image.save
        
        image = ShopifyAPI::Image.new
        image.src = "http://#{request.host_with_port}#{product_image.product_image.xlarge.url}"
        product.images.push image
        product.save
        
        format.html { redirect_to "/makers/#{maker.slug}/store", notice: 'Product was successfully created.' }
      else
        format.html { redirect_to "/makers/#{maker.slug}/store", notice: product.errors }
      end
    end
  end
  
  def new_image
  end

  def show
  end

  def edit
    @maker = current_user.maker
    @product = ShopifyAPI::Product.find(params[:id])
    opt1Arr = Array[]
		opt2Arr = Array[]
		opt3Arr = Array[]
		@product.variants.each do |pv|
			opt1Arr.push pv.option1
			opt2Arr.push pv.option2 unless pv.option2.blank?
			opt3Arr.push pv.option3 unless pv.option3.blank?
		end
		@opt1_values = opt1Arr.uniq
		@opt2_values = opt2Arr.uniq
		@opt3_values = opt3Arr.uniq
  end
  
  def update
    maker = current_user.maker
    product = ShopifyAPI::Product.find(params[:id])
    product.title = params[:title]
    product.product_type = params[:product_type]
    product.body_html = params[:body_html]
    product.tags = params[:tags]
    product.vendor = params[:vendor]
    
    respond_to do |format|
      if product.save
        current_user = maker.user
        format.html { redirect_to "/makers/#{maker.slug}/store", notice: 'Product was successfully updated.' }
      else
        format.html { redirect_to "/makers/#{maker.slug}/store", notice: product.errors }
      end
    end
  end
  
  def delete
    maker = current_user.maker
    product = ShopifyAPI::Product.find(params[:id])
    product.destroy
    respond_to do |format|
      format.html { redirect_to "/makers/#{maker.slug}/store" }
    end
  end
  
  #options
  def update_options
    maker = current_user.maker
    product = ShopifyAPI::Product.find(params[:product_id])
    
    # hackey way of avoiding 'nil name' error
    # one option
    options = [{"name" => params[:option_name_1]}]
    # two options
    unless params[:option_name_2].blank?
      options = [{"name" => params[:option_name_1]},{"name" => params[:option_name_2]}]
    end
    # three options
    unless params[:option_name_3].blank?
      options = [{"name" => params[:option_name_1]},{"name" => params[:option_name_2]},{"name" => params[:option_name_3]}] 
    end
    product.options = options
    
    product.variants.each do |pv|
      unless params[:option_value_1].blank?
        pv.option1 = params[:option_value_1]
      end
      unless params[:option_value_2].blank?
        pv.option2 = params[:option_value_2]
      end
      unless params[:option_value_3].blank?
        pv.option3 = params[:option_value_3]
      end
    end
    
    respond_to do |format|
      if product.save
        format.html { redirect_to "/makers_admin/stores/#{product.id}/edit", notice: 'Product Options were successfully updated.' }
      else
        format.html { redirect_to "/makers_admin/stores/#{product.id}/edit", notice: product.errors }
      end
    end
  end
  
  # VARIANTS
  def new_variant
    maker = current_user.maker
    variant = ShopifyAPI::Variant.new
    variant.option1 = params[:option1]
    variant.option2 = params[:option2]
    variant.option3 = params[:option3]
    variant.price = params[:price]
    variant.weight = params[:weight]
    if !params[:track_inventory].nil?
      variant.inventory_management = "shopify"
      variant.inventory_quantity = params[:inventory_quantity]
    else
      variant.inventory_management = ""
      variant.inventory_quantity = "1"
    end
    variant.sku = maker.slug + "-" + params[:sku]

    product = ShopifyAPI::Product.find(params[:id])
    product.variants.push variant
    product.save

    respond_to do |format|  
      if product.save
        format.html { redirect_to "/makers_admin/stores/#{product.id}/edit", notice: "Variant was successfully added." }
      else
        format.html { redirect_to "/makers_admin/stores/#{product.id}/edit", notice: product.errors }
      end
    end   
  end
  
  def update_variant
    maker = current_user.maker
    product = params[:product_id]
    variant = ShopifyAPI::Variant.find(params[:variant_id])
    variant.option1 = params[:option1]
    variant.option2 = params[:option2]
    variant.option3 = params[:option3]
    variant.price = params[:price]
    variant.weight = params[:weight]
    if !params[:track_inventory].nil?
      variant.inventory_management = "shopify"
      variant.inventory_quantity = params[:inventory_quantity]
    else
      variant.inventory_management = ""
      variant.inventory_quantity = "1"
    end
    variant.sku = params[:sku]
    variant.save
    respond_to do |format|
      if variant.save
        current_user = maker.user
        format.html { redirect_to "/makers_admin/stores/#{product}/edit", notice: "Variant was successfully updated." }
      else
        format.html { redirect_to "/makers_admin/stores/#{product}/edit", notice: variant.errors }
      end
    end
  end
  
  def delete_variant
    maker = current_user.maker
    variant = ShopifyAPI::Variant.find(params[:variant_id])
    variant.destroy
    respond_to do |format|
      format.html { render :text => '{"success" : "true" }' }
    end
  end

  # IMAGES
  def images
    product_image = ProductImage.new(params[:product_image])
    product_image.product_id = params[:product_id]
    product_image.save
    
    image = ShopifyAPI::Image.new
    product = ShopifyAPI::Product.find(params[:product_id])
    image.src = "http://#{request.host_with_port}#{product_image.product_image.xlarge.url}"
    product.images.push image
    product.save
    
    redirect_to "/makers_admin/stores/#{params[:product_id]}/edit"
  end
  
  def delete_image
    image = ShopifyAPI::Image.find(params[:image_id], :params => {:product_id => params[:product_id]})
    image.destroy
    respond_to do |format|
      format.html { render :text => '{"success" : "true"}'}
    end
  end
  
  def orders
    @maker = current_user.maker
    @vendor = @maker.id < 10 ? "0#{@maker.id}" : "#{@maker.id}"
    @orders = Order.order("order_date DESC")
    @items = OrderItem.where("item_vendor = ?", @vendor)
  end
  
  def order
    @maker = current_user.maker
    @vendor = @maker.id < 10 ? "0#{@maker.id}" : "#{@maker.id}"
    @order = Order.find(params[:id])
    @items = OrderItem.where("order_id = ? AND item_vendor = ?", @order, @vendor)
  end

  private
    def set_nav_states
      @storeActive = "active"
    end
    
    def get_product_options
      products = ShopifyAPI::Product.find(:all)
      ptype = Array[]
      pvendors = Array[]
      ptags = Array[]
      poptions = Array[]
      products.each do |p|
        ptype.push p.product_type
        pvendors.push p.vendor
        unless p.tags.blank?
          tags = p.tags.split(",")
          tags.each do |t|
            ptags.push t.strip
          end
        end
        p.options.each do |o|
          poptions.push o.name
        end
      end
      @types = ptype.uniq
      @vendors = pvendors.uniq
      @tags = ptags.uniq.sort
      @options = poptions.uniq
    end
end
