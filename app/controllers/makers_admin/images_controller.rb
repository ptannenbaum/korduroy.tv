class MakersAdmin::ImagesController < ApplicationController
  before_filter :authenticate_user!, :set_nav_states
  load_and_authorize_resource
  layout 'application'
  
  def index
    @blog = Blog.find params[:blog_id]
    
    respond_to do |format|
      format.html { render :layout => false }
      format.js { render :js => "var tinyMCEImageList = #{Image.image_list(@images).to_json};" }
    end
  end
  
  def new
    respond_to do |format|
      format.html { render :layout => false }
    end
  end

  def edit
  end

  def create
    @image.blog_id = params[:blog_id]
    @image.image = params[:file]
    respond_to do |format|
      if @image.save
        #format.html { redirect_to [:makers_admin, @image], notice: 'Image was successfully created.' }
        format.html { render :text => '<script type="text/javascript" language="javascript">self.close();</script>'}
        format.json { render json: @image, status: :created, location: @image }
      else
        format.html { render action: "new" }
        format.json { render json: @image.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    respond_to do |format|
      if @image.update_attributes(params[:image])
        format.html { redirect_to [:makers_admin, @image], notice: 'Image was successfully updated.' }
        format.json { head :ok }
      else
        format.html { render action: "edit" }
        format.json { render json: @image.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @image.destroy

    respond_to do |format|
      #format.html { redirect_to edit_makers_admin_blog_path(@image.blog_id) }
      format.html { render :text => '{"success" : "true"}' }
      #format.json { head :ok }
    end
  end
  
  private
     def set_nav_states
       @imageActive = "active"
     end
end
