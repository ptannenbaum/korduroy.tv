class MakersAdmin::MakersController < ApplicationController
  before_filter :authenticate_user!, :set_nav_states
  load_and_authorize_resource
  layout 'application'
  
  def edit
    @maker = current_user.maker
  end

  def update
    unless params[:avatar].blank?
      @maker.user.avatar = (params[:avatar]);
      @maker.user.save
    end
    respond_to do |format|
      if @maker.update_attributes(params[:maker])
        format.html { redirect_to "/makers/#{@maker.slug}/bio", notice: 'Profile was successfully updated.' }
        format.json { head :ok }
      else
        format.html { render action: "edit" }
        format.json { render json: @maker.errors, status: :unprocessable_entity }
      end
    end
  end
  
  private
     def set_nav_states
       @bioActive = "active"
     end
end
