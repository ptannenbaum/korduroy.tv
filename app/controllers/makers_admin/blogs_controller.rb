module MakersAdmin
  class BlogsController < ApplicationController
    before_filter :authenticate_user!, :set_nav_states, :load_maker
    load_and_authorize_resource
    layout 'application'
  
    def new
    end

    def create 
      @blog.user = current_user
      respond_to do |format|
        if @blog.save
          format.html { redirect_to "/makers/#{@maker.slug}/blog/#{@blog.created_at.year}/#{@blog.slug}/#{@blog.id}", notice: 'Post was successfully created.' }
          format.json { render json: @blog, status: :created, location: @blog }
        else
          format.html { render action: "new" }
          format.json { render json: @blog.errors, status: :unprocessable_entity }
        end
      end
    end
    
    def edit
      Blog.find(params["id"])
    end

    def update
      respond_to do |format|
        if @blog.update_attributes(params[:blog])
          format.html { redirect_to "/makers/#{@maker.slug}/blog/#{@blog.created_at.year}/#{@blog.slug}/#{@blog.id}", notice: 'Post was successfully updated.' }
          format.json { head :ok }
        else
          format.html { render action: "edit" }
          format.json { render json: @blog.errors, status: :unprocessable_entity }
        end
      end
    end

    def destroy
      @blog.destroy

      respond_to do |format|
        format.html { redirect_to "/makers/#{@maker.slug}" }
        format.json { head :ok }
      end
    end
    
    def reblog
      @blog.reblog!

      respond_to do |format|
        format.html { redirect_to "/makers/#{@blog.user.maker.slug}/blog/#{@blog.created_at.year}/#{@blog.slug}/#{@blog.id}" }
        format.json { head :ok }
      end
    end
    
    private
       def set_nav_states
         @blogActive = "active"
       end
       
       def load_maker
         @maker = current_user.maker
       end
       
  end
end