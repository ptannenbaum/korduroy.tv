class MakersAdmin::MakerPhotosController < ApplicationController
  before_filter :authenticate_user!, :set_nav_states
  load_and_authorize_resource
  layout 'application'
  
  def new
    @maker = current_user.maker
  end

  def edit
    @maker = current_user.maker
  end

  def create
    @maker_photo.maker = current_user.maker
    respond_to do |format|
      if @maker_photo.save
        format.html { redirect_to "/makers/#{@maker_photo.maker.slug}/gallery", notice: 'Photo was successfully created.' }
        format.json { render json: @maker_photo, status: :created, location: @maker_photo }
      else
        format.html { render action: "new" }
        format.json { render json: @maker_photo.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    respond_to do |format|
      if @maker_photo.update_attributes(params[:maker_photo])
        format.html { redirect_to "/makers/#{@maker_photo.maker.slug}/gallery", notice: 'Photo was successfully updated.' }
        format.json { head :ok }
      else
        format.html { render action: "edit" }
        format.json { render json: @maker_photo.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @maker_photo.destroy

    respond_to do |format|
      format.html { redirect_to "/makers/#{@maker_photo.maker.slug}/gallery" }
      format.json { head :ok }
    end
  end
  
  private
     def set_nav_states
       @galleryActive = "active"
     end
  
end
