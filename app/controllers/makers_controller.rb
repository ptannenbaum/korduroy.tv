class MakersController < ApplicationController
  ShopifyAPI::Base.site = "https://3fd9effe5aef663e58dfd78a918e3ef2:741f4006631fd33228e87ef4fd18d69b@korduroy.myshopify.com/admin"
  ShopifyAPI::Base.format = :xml
  
  before_filter :load_categories, :set_nav_states
  
  def index
    #@user = User.where(:default_blogger => false).where('maker_id >= 0')
    #@makers = Maker.includes(:maker_category, :maker_photos)
    @maker_posts = Blog.maker_blog_posts.page(params[:page]).per(30)
  end
  
  def list
    @makers = User.where('maker_id >= 0').shuffle
    #@makers = Maker.where(:visible => true).page params[:page]
  end
  
  def rss
    @maker = Maker.find_by_slug(params[:slug])
    @blogs = Blog.where(:user_id => @maker.user.id).limit(10).order('updated_at DESC')
    respond_to do |format|
      format.xml
    end
  end
  
  def category
    @category = MakerCategory.find_by_slug!(params[:slug])
    @makers = @category.makers.where(:visible => true)
  end
  
  def show
    @maker = Maker.find_by_slug!(params[:slug])
    @blogs = @maker.user.blogs.page params[:page]
    @blogActive = "active"
  end
  
  def blog
    @maker = Maker.find_by_slug!(params[:slug])
    #user = User.where("maker_id = ?", @maker.id)
    @post = Blog.find(params[:id])
    @comments = @post.comments
    @current_user = current_user
    @blogActive = "active"
    @blogDetail = true
    @products = ShopifyAPI::Product.find(:all, :params => {:collection_id => 4503832, :published_status => "published"})
    @prod_array = Array[]
    @products.each do |p|
      @post.tags.each do |t|
        unless p.tags.blank?
          if p.tags.downcase.include? t.to_s.downcase
            @prod_array.push p
          end
        end
      end
    end
    @related_products = @prod_array.uniq
    @related_shows = Show.tagged_with(@post.tags, :any => true)
    @related_blogs = Blog.where('slug <> ?', @post.slug).tagged_with(@post.tags, :any => true)
    @related_items = @related_products + @related_shows + @related_blogs
  end
  
  def comment
    @maker = Maker.find_by_slug!(params[:slug])
    @post = Blog.find_by_slug_and_visible!(params[:post_slug], true)
    @comment = @post.comments.create(params[:comment])
    @current_user = current_user
    @comment.user = current_user
    @blogActive = "active"
    @blogDetail = true
    respond_to do |format|
      if @comment.save
        format.html { redirect_to url_for(:slug => @maker.slug, :year => @post.created_at.year, :post_slug => @post.slug), notice: 'Thanks for your comments.' }
        format.json { render json: @comment, status: :created, location: @post }
      else
        format.html { render action: "new" }
        format.json { render json: @comment.errors, status: :unprocessable_entity }
      end
    end
  end
  
  def store
    @maker = Maker.find_by_slug_and_storefront!(params[:slug], true)
    @products = ShopifyAPI::Product.find(:all, :params => {:collection_id => @maker.collection_id, :published_status => "published"})
    @hero = @products.first
    @products = @products.from(1)
    @storeActive = "active"
  end
  
  def product
    @maker = Maker.find_by_slug_and_storefront!(params[:slug], true)
    @product = ShopifyAPI::Product.find(params[:product])
    @storeActive = "active"
    tagArray = Array[]
    unless @product.tags.blank?
      tags = @product.tags.split(",")
      tags.each do |t|
        tagArray.push t.strip
      end
    end
    @tags = tagArray
    
    if @product.variants.size > 1
      v1 = Array[]
      v2 = Array[]
      v3 = Array[]
      @product.variants.each do |v|
        v1.push v.option1
        v2.push v.option2
        v3.push v.option3
      end
      @v1 = v1.uniq
      @v2 = v2.uniq
      @v3 = v3.uniq
    end
    
    @products = ShopifyAPI::Product.find(:all, :params => {:published_status => "published"})
    prod_array = Array[]
    @products.each do |p|
      unless p.id == @product.id
        unless tags.blank?
          tags.each do |t|
            unless p.tags.blank?
              if p.tags.downcase.include? t.to_s.downcase
                prod_array.push p
              end
            end
          end
        end
      end
    end
    @related_products = prod_array.uniq.sort_by{ rand }.slice(0..4)
    @related_shows = Show.tagged_with(@tags, :any => true).limit(5)
    @related_blogs = Blog.tagged_with(@tags, :any => true).limit(5)
    @related_items = @related_products + @related_shows + @related_blogs
  end
  
  def bio
    @maker = Maker.find_by_slug!(params[:slug])
    @bioActive = "active"
  end
  
  def gallery
    @maker = Maker.find_by_slug!(params[:slug])
    @videos = MakerVideo.where(:maker_id => @maker).order("created_at desc")
    @photos = MakerPhoto.where(:maker_id => @maker).order("created_at desc")
    @galleryActive = "active"
  end
  
private
  def load_categories
    @categories = MakerCategory.all
  end

  def set_nav_states
    @ktvMakersActive = "active"
  end
end
