class ShowsController < ApplicationController
  ShopifyAPI::Base.site = "https://3fd9effe5aef663e58dfd78a918e3ef2:741f4006631fd33228e87ef4fd18d69b@korduroy.myshopify.com/admin"

  def index
    load_categories
  end
  
  def category
    load_categories
    set_nav_states
    @category = ShowCategory.where("slug = ?", params[:slug]).includes(:shows => [:credits]).first
    @featured = Show.where(:staff_pick => true, :show_category_id => @category).includes(:credits)
    @hero = @featured.first
  end
  
  def show
    load_categories
    set_nav_states
    @hero = Show.find_by_slug(params[:show_slug])
    @hero.increment_impressions
    @comments = @hero.comments.all
    @current_user = current_user
    @products = ShopifyAPI::Product.find(:all, :params => {:collection_id => 4503832, :published_status => "published"})
    prod_array = Array[]
    @products.each do |p|
      unless @hero.tags.blank?
        @hero.tags.each do |t|
          unless p.tags.blank?
            if p.tags.downcase.include? t.to_s.downcase
              prod_array.push p
            end
          end
        end
      end
    end
    @related_products = prod_array.uniq.sort_by{ rand }.slice(0..4)
    @related_blogs = Blog.tagged_with(@hero.tags, :any => true).limit(5)
    @related_shows = Show.where('slug <> ?', @hero.slug).tagged_with(@hero.tags, :any => true).limit(5)
    @related_items = @related_products + @related_blogs + @related_shows
    @full = (params[:tooltip] || params[:preview])? false : true
  end
  
  def comment
    @show = Show.find_by_slug!(params[:show_slug])
    @comment = @show.comments.create(params[:comment])
    @comment.user = current_user
    respond_to do |format|
      if @comment.save
        format.html { redirect_to url_for(:slug => @show.show_category.slug, :show_slug => @show.slug), notice: 'Thanks for your comments.' }
        format.json { render json: @comment, status: :created, location: @poll }
      else
        format.html { render action: "new" }
        format.json { render json: @comment.errors, status: :unprocessable_entity }
      end
    end
  end
  
  def rate
    @show = Show.find_by_slug!(params[:show_slug])
    respond_to do |format|
      if @show.rate_it(params[:rating], current_user)
        format.html { render :text => '{"success" : "true"}'}
      else
        format.html { render :text => '{"success" : "false"}'}
      end
    end
  end
  
  private

    def set_nav_states
      @ktvShowsActive = "active"
      @swidth = "960"
    end
  
    def load_categories
      @categories = ShowCategory.includes(:shows)
    end
end
