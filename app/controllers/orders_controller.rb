class OrdersController < ApplicationController
  ShopifyAPI::Base.site = "https://3fd9effe5aef663e58dfd78a918e3ef2:741f4006631fd33228e87ef4fd18d69b@korduroy.myshopify.com/admin"
  
  #before_filter :verify_webhook, :except => 'verify_webhook'
  skip_filter :verify_authenticity_token
  
  def new
    data = ActiveSupport::JSON.decode(request.body.read)
    order_id = data["id"]
    existing_order = Order.where("order_id = ?", order_id)
    if existing_order.blank?
      order = Order.new(
        :order_id => data["id"],
        :order_number => data["number"],
        :order_name => data["name"],
        :order_financial_status => data["financial_status"],
        :order_fulfillment_status => data["fulfillment_status"],
        :order_note => data["note"],
        :order_date => data["created_at"],
        :order_cancel_date => data["cancelled_at"],
        :order_cancel_reason => data["cancel_reason"],
        :order_close_date => data["closed_at"],
      
        :customer_email => data["email"],
      
        :customer_billing_name => data["billing_address"]["name"],
        :customer_billing_phone => data["billing_address"]["phone"],

        :customer_shipping_name => data["shipping_address"]["name"],
        :customer_shipping_phone => data["shipping_address"]["phone"],
        :customer_shipping_address1 => data["shipping_address"]["address1"],
        :customer_shipping_address2 => data["shipping_address"]["address2"],
        :customer_shipping_city => data["shipping_address"]["city"],
        :customer_shipping_province => data["shipping_address"]["province_code"],
        :customer_shipping_zip => data["shipping_address"]["zip"],
        :customer_shipping_country => data["shipping_address"]["country"]
      )
      order.save
    
      data["line_items"].each do |i|
        vendor = i["vendor"]
        maker = Maker.where(:id => vendor).limit(1)
        unless maker.blank?
          item = OrderItem.new(
            :order_id => order.id,
            :maker_id => maker.id,
            :item_id => i["id"],
            :item_product_id => i["product_id"],
            :item_name => i["name"],
            :item_qty => i["quantity"],
            :item_price => i["price"],
            :item_vendor => i["vendor"],
            :item_sku => i["sku"],
            :item_variant_id => i["variant_id"],
            :item_weight => i["grams"],
            :item_fulfillment_status => i["fulfillment_status"],
          )
          item.save
        end
      end
    end
    head :ok
  end

  def update
  end

  def fulfill
    maker = current_user.maker
    ktv_order = Order.find(params[:ktv_order_id])
    ktv_items = params[:line_items]
    
    fulfillment = ShopifyAPI::Fulfillment.new
    fulfillment.prefix_options[:order_id] = params[:order_id]
    items = []
    ktv_items.each do |item|
      item = {"id" => item}
      items.push item
    end
    fulfillment.line_items = items
    fulfillment.tracking_number = params[:tracking_number]
    
    respond_to do |format|
      if fulfillment.save
        # update db with fulfillment status
        ktv_order.update_attribute(:order_fulfillment_status, "partial")
        ktv_items.each do |item|
          i = OrderItem.where("item_id = ?", item).first
          i.update_attribute(:item_fulfillment_status, "shipped")
        end
        
        format.html { redirect_to "/makers_admin/stores/order/#{ktv_order.id}", notice: 'Items have been fulfilled.' }
      else
        format.html { redirect_to "/makers_admin/stores/order/#{ktv_order.id}", notice: fulfillment.errors }
      end
    end
  end
  
  private
  
  def verify_webhook
  #  data = request.body.read.to_s
  #  hmac_header = request.headers['HTTP_X_SHOPIFY_HMAC_SHA256']
  #  digest  = OpenSSL::Digest::Digest.new('sha256')
  #  calculated_hmac = Base64.encode64(OpenSSL::HMAC.digest(digest, KorduroyTv::Application.config.shopify.secret, data)).strip
  #  unless calculated_hmac == hmac_header
  #    head :unauthorized
  #  end
  #  request.body.rewind
  end

end
