class BlogsController < ApplicationController
  before_filter :load_categories, :set_nav_states
  
  def index
    @ktv_posts = Blog.ktv_blog_posts.page(params[:page]).per(10)
  end
  
  def show
    @user = User.where(:default_blogger => true)
    @blog = Blog.find_by_slug_and_year(params[:slug], params[:year]).
      first
    throw ActiveRecord::RecordNotFound unless @blog
    @comments = @blog.comments.all
    @current_user = current_user
    #@comment.user = current_user
    @products = ShopifyAPI::Product.find(:all, :params => {:collection_id => 4503832, :published_status => "published"})
    @prod_array = Array[]
    @products.each do |p|
      unless @blog.tags.blank?
        @blog.tags.each do |t|
          unless p.tags.blank?
            if p.tags.downcase.include? t.to_s.downcase
              @prod_array.push p
            end
          end
        end
      end
    end
    @related_products = @prod_array.uniq.sort_by{ rand }.slice(0..4)
    @related_shows = Show.tagged_with(@blog.tags, :any => true).limit(5)
    @related_blogs = Blog.where('slug <> ?', @blog.slug).tagged_with(@blog.tags, :any => true).limit(5)
    @related_items = @related_products + @related_shows + @related_blogs
  end
  
  def category
    @category = BlogCategory.find_by_slug!(params[:slug])
    @ktv_posts = @category.blogs.where(:visible => true).order('created_at DESC').page(params[:page]).per(10)
  end
  
  def comment
    @user = User.where(:default_blogger => true)
    @blog = Blog.find_by_slug_and_year(params[:slug], params[:year]).
      first
    @comment = @blog.comments.create(params[:comment])
    @comment.user = current_user
    respond_to do |format|
      if @comment.save
        format.html { redirect_to url_for(:year => @blog.created_at.year, :slug => @blog.slug), notice: 'Thanks for your comments.' }
        format.json { render json: @comment, status: :created, location: @poll }
      else
        format.html { render action: "new" }
        format.json { render json: @comment.errors, status: :unprocessable_entity }
      end
    end
  end
  
  def rss
    @ktv_posts = Blog.ktv_blog_posts.page(params[:page]).per(10)
    respond_to do |format|
      format.xml
    end
  end

  private

    def load_categories
      @categories = BlogCategory.all
    end

    def set_nav_states
      @ktvBlogActive = "active"
    end
end
