class PagesController < ApplicationController
  
  def about
    @maker = User.where("maker_id >= 0")
    @crew = Maker.where(:crew => true)
  end
  
  def projects
    @projects = Project.order("start_date DESC")
  end      
  
  def privacy
  end
  
  def terms
  end
  
  def submissions
  end
  
  def returns
  end
  
  def club
  end
  
  def help
  end

  def reef_x_cyrus
  end

  def compassing
  end
end
