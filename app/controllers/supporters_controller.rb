class SupportersController < ApplicationController
  before_filter :set_nav_states
  def index
    @supporters = Supporter.all
  end

  def show
    @category   = SupporterCategory.find_by_slug(params[:id])
    @supporters  = @category.supporters
    
    respond_to do |format|
      format.html { render action: "index" }
    end
  end

  private

    def set_nav_states
      @ktvSupportersActive = "active"
    end
end
