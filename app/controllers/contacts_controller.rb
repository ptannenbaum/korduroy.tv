class ContactsController < ApplicationController
  def new
    @contact = Contact.new
    
    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @contact }
    end
  end

  # POST /admin/polls
  # POST /admin/polls.json
  def create
    @contact = Contact.new(params[:contact])
    @contact.ip = request.remote_ip
    
    respond_to do |format|
      if verify_recaptcha && @contact.save
        ContactMailer.site_contact(@contact).deliver
        format.html { redirect_to contact_thanks_url, notice: 'Your message has been delivered.' }
        format.json { render json: @contact, status: :created, location: @poll }
      else
        format.html { render action: "new" }
        format.json { render json: @contact.errors, status: :unprocessable_entity }
      end
    end
  end
end
