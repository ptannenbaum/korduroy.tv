class ContactMailer < ActionMailer::Base
  default from: 'noreply@korduroy.tv'
  
  def site_contact(contact)
    @contact = contact
    mail(:to => 'info@korduroy.tv', :subject => '[Korduroy.TV] New contact')
  end
  
  def maker_contact(maker_contact)
    @maker_contact = maker_contact
    mail(:to => @maker_contact.maker.user.email, :subject => '[Korduroy.TV] New contact')
  end
end
