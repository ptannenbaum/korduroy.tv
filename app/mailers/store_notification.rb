class StoreNotification < ActionMailer::Base
  default from: 'noreply@korduroy.tv'

  def new_purchase(order_item)
    @order_item = order_item
    @maker      = order_item.maker

    mail to: @maker.user.email
  end
end
