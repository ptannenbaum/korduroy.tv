xml.instruct! 

xml.rss "version" => "2.0", "xmlns:dc" => "http://purl.org/dc/elements/1.1/" do 
  xml.channel do 
  
    xml.title "#{@maker.name} Blog Feed" 
    xml.link maker_url(@maker.slug)
    #xml.description "Modulor Daily News" 
    
    @blogs.each do |blog| 
      xml.item do 
        xml.title blog.title
        xml.link makers_blog_url(:year => blog.created_at.year, :slug => @maker.slug, :post_slug => blog.slug)
        xml.description blog.content 
        xml.guid makers_blog_url(:year => blog.created_at.year, :slug => @maker.slug, :post_slug => blog.slug)
      end 
    end 
    
  end 
end