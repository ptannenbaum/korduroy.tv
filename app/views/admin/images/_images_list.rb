<ul class="imagesList">
	<% @project.images.each do |image| %>
	<li>
		<img src="<%= image.image.url %>" class="inlinePostImage" data-projectid="" data-imageid="">
		<a href="/about/projects/<%= @project.id %>/images/<%= image.id %>" class="button delete">Delete</a>
	</li>
	<% end %>
</ul>