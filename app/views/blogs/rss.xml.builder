xml.instruct! 

xml.rss "version" => "2.0", "xmlns:dc" => "http://purl.org/dc/elements/1.1/" do 
  xml.channel do 
  
    xml.title "Korduroy.TV Blog Feed" 
    xml.link "http://korduroy.tv/blogs"
    #xml.description "Modulor Daily News" 
    
    @ktv_posts.each do |blog| 
      xml.item do 
        xml.title blog.title
        xml.link main_blog_path(:year => blog.created_at.year, :slug => blog.slug)
        xml.description blog.content 
        xml.guid main_blog_path(:year => blog.created_at.year, :slug => blog.slug)
      end 
    end 
    
  end 
end