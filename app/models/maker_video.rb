# == Schema Information
#
# Table name: maker_videos
#
#  id              :integer          not null, primary key
#  title           :string(255)
#  description     :text
#  visible         :boolean          default(TRUE)
#  comments_closed :boolean
#  vimeo           :string(255)      not null
#  maker_id        :integer
#  created_at      :datetime
#  updated_at      :datetime
#  youtube         :string(255)
#  thumbnail       :string(255)
#

class MakerVideo < ActiveRecord::Base
  belongs_to :maker
  
  validates_presence_of :title
  validates_presence_of :vimeo,   :if => proc { |video| video['youtube'].blank? }, :message => 'must enter a Vimeo or YouTube video'
  validates_presence_of :youtube, :if => proc { |video| video['vimeo'].blank? }, :message => 'must enter a Vimeo or YouTube video'

end
