# == Schema Information
#
# Table name: maker_contacts
#
#  id         :integer          not null, primary key
#  name       :string(255)      not null
#  email      :string(255)      not null
#  message    :text             not null
#  maker_id   :integer          not null
#  ip         :string(255)      not null
#  created_at :datetime
#  updated_at :datetime
#

class MakerContact < ActiveRecord::Base
  validates_presence_of :name, :email, :message, :maker
  validates_format_of   :email, :with => /\b[A-Z0-9._%a-z\-]+@(?:[A-Z0-9a-z\-]+\.)+[A-Za-z]{2,4}\z/
  
  belongs_to :maker
end
