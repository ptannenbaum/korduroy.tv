# == Schema Information
#
# Table name: credit_types
#
#  id         :integer          not null, primary key
#  name       :string(255)
#  created_at :datetime
#  updated_at :datetime
#

class CreditType < ActiveRecord::Base
  validates_presence_of :name
  
  has_many :credits
end
