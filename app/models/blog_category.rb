# == Schema Information
#
# Table name: blog_categories
#
#  id            :integer          not null, primary key
#  name          :string(255)
#  slug          :string(255)
#  created_at    :datetime
#  updated_at    :datetime
#  display_order :integer
#

class BlogCategory < ActiveRecord::Base
  validates_presence_of :name, :slug
  validates_uniqueness_of :slug, :display_order
  validates_format_of     :slug, :with => /\A[-a-z0-9]+\Z/i, :message => 'is invalid, only dashes (-) and alphanumeric charachters are allowed'
  
  has_many :blogs
  
  default_scope :order => 'display_order ASC'
end
