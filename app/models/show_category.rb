# == Schema Information
#
# Table name: show_categories
#
#  id            :integer          not null, primary key
#  title         :string(255)
#  description   :text
#  icon          :string(255)
#  banner        :string(255)
#  slug          :string(255)
#  created_at    :datetime
#  updated_at    :datetime
#  display_order :integer
#

class ShowCategory < ActiveRecord::Base
  validates_presence_of :title, :slug, :description
  validates_uniqueness_of :slug, :display_order
  validates_format_of     :slug, :with => /\A[-a-z0-9]+\Z/i, :message => 'is invalid, only dashes (-) and alphanumeric charachters are allowed'
  
  has_many :shows
  
  default_scope :order => 'display_order ASC'
  
  mount_uploader :banner, BannerUploader
  mount_uploader :icon, IconUploader
end
