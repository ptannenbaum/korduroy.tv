# == Schema Information
#
# Table name: polls
#
#  id               :integer          not null, primary key
#  question         :string(255)
#  created_at       :datetime
#  updated_at       :datetime
#  active           :boolean          default(FALSE)
#  poll_category_id :integer
#  comments_closed  :boolean
#  comments_count   :integer
#

class Poll < ActiveRecord::Base
  #validates_presence_of :answer
  
  belongs_to :poll_category
  
  has_many :answers, :order => "position"
  
  acts_as_commentable
  
  accepts_nested_attributes_for :answers, :allow_destroy => true, :reject_if => :all_blank
  
  def self.user_already_voted? user, poll
    PollResponse.where(:user_id => user.id, :poll_id => poll.id).count > 0
  end
  
  def tally_responses
    Answer.unscoped.
      select("answers.*, (select sum(position) from poll_responses where answer_id = answers.id) as votes, (select sum(position) from poll_responses WHERE poll_id = #{id}) as total_votes").
      where('poll_id = ?', id).
      order('votes DESC')
  end
  
  def user_vote user, poll
    Answer.unscoped.
      select("answers.*, (select position from poll_responses where answer_id = answers.id and user_id = #{user.id}) as vote, (select created_at from poll_responses where answer_id = answers.id and user_id = #{user.id}) as vote_date").
      where('poll_id = ?', poll.id).
      order('vote DESC')
  end
end
