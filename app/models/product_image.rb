# == Schema Information
#
# Table name: product_images
#
#  id            :integer          not null, primary key
#  product_image :string(255)      not null
#  product_id    :integer          not null
#  created_at    :datetime
#  updated_at    :datetime
#

class ProductImage < ActiveRecord::Base
  mount_uploader :product_image, ProductImageUploader
end
