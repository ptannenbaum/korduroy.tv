# == Schema Information
#
# Table name: projects
#
#  id         :integer          not null, primary key
#  title      :string(255)
#  slug       :string(255)
#  body       :text
#  created_at :datetime
#  updated_at :datetime
#  start_date :date
#  end_date   :date
#

class Project < ActiveRecord::Base
  validates_presence_of :title, :start_date, :body
end
