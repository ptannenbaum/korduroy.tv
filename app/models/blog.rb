# == Schema Information
#
# Table name: blogs
#
#  id               :integer          not null, primary key
#  title            :string(255)
#  content          :text
#  slug             :string(255)
#  visible          :boolean          default(TRUE)
#  comments_closed  :boolean
#  user_id          :integer
#  created_at       :datetime
#  updated_at       :datetime
#  blog_category_id :integer
#  post_image       :string(255)
#  url              :string(255)
#  comments_count   :integer
#  reblogged_at     :datetime
#  wp_post_id       :integer
#  featured         :boolean          default(FALSE)
#

class Blog < ActiveRecord::Base
  belongs_to :user
  belongs_to :blog_category
  has_many :images
  acts_as_commentable
  acts_as_taggable
  
  accepts_nested_attributes_for :comments, :allow_destroy => true, :reject_if => proc { |a| a['title'].blank? and a['comment'].blank? }
  accepts_nested_attributes_for :images, :allow_destroy => true, :reject_if => :all_blank
  
  validates_format_of     :slug, :with => /\A[-a-z0-9]+\Z/i, :message => 'is invalid, only dashes (-) and alphanumeric charachters are allowed'
  validates_presence_of   :title, :content, :slug
  validates_presence_of   :post_image, :if => proc { |blog| blog['url'].blank? }
  validates_uniqueness_of :url, :scope => :user_id
  validates_uniqueness_of :slug
  
  attr_protected  :user_id, :reblogged_at
  
  default_scope :order => 'created_at DESC'
  
  mount_uploader :post_image, PostImageUploader
  
  paginates_per 10
  
  def self.find_by_slug_and_year(slug, year)
    Blog.where(:slug => slug).
      where('extract(year from created_at) = ?', year).
      where(:visible => true).
      limit(1)
  end
  
  def self.ktv_blog_posts
    self.where('(user_id in (select id from users where default_blogger = true) or reblogged_at is not null)', @user).
      where(:visible => true).
      order('case when reblogged_at is null then created_at else reblogged_at end desc')
  end
  
  def self.maker_blog_posts
    @user = User.where(:default_blogger => false).joins(:maker).where(:makers => {:visible => true})
    self.where(:user_id => @user, :visible => true)
  end
  
  def rebloggable?
    reblogged_at.nil?
  end
  
  # Reblogging allows an administrator to reblog this blog to the main blog section.
  def reblog!
    self.reblogged_at = Time.now
    save
  end
end
