# == Schema Information
#
# Table name: shows
#
#  id               :integer          not null, primary key
#  created_at       :datetime
#  updated_at       :datetime
#  vimeo            :string(255)
#  youtube          :string(255)
#  thumbnail        :string(255)
#  show_category_id :integer
#  title            :string(255)
#  description      :text
#  comments_closed  :boolean
#  staff_pick       :boolean
#  slug             :string(255)
#  impressions      :integer          default(0)
#  comments_count   :integer
#  featured         :boolean          default(FALSE)
#

class Show < ActiveRecord::Base
  belongs_to  :show_category
  has_many    :credits
  has_many    :featured_makers
  has_many    :makers,  :through => :featured_makers
  acts_as_commentable
  acts_as_taggable
  acts_as_rateable
  
  validates_format_of :slug, :with => /\A[-a-z0-9]+\Z/i, :message => 'is invalid, only dashes (-) and alphanumeric charachters are allowed'
  validates_presence_of :title, :slug, :description, :show_category, :vimeo
  
  accepts_nested_attributes_for :credits, :allow_destroy => true, :reject_if => :all_blank
  accepts_nested_attributes_for :comments, :allow_destroy => true, :reject_if => proc { |a| a['title'].blank? and a['comment'].blank? }
  
  #mount_uploader :thumbnail, ThumbnailUploader
  default_scope :order => 'created_at DESC'
  scope :top_rated,   order('impressions DESC')
  scope :newest,      order('created_at DESC')
  scope :staff_pick,  where(:staff_pick => true)
  
  def category
    show_category if show_category
  end

  def increment_impressions
    Show.increment_counter(:impressions, id)
  end
  
end
