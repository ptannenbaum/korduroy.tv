# == Schema Information
#
# Table name: orders
#
#  id                         :integer          not null, primary key
#  order_id                   :integer
#  order_number               :integer
#  order_name                 :string(255)
#  order_financial_status     :string(255)
#  order_fulfillment_status   :string(255)
#  order_note                 :text
#  order_date                 :datetime
#  order_cancel_date          :datetime
#  order_cancel_reason        :string(255)
#  order_close_date           :datetime
#  customer_email             :string(255)
#  customer_billing_name      :string(255)
#  customer_billing_phone     :string(255)
#  customer_shipping_name     :string(255)
#  customer_shipping_email    :string(255)
#  customer_shipping_phone    :string(255)
#  customer_shipping_address1 :string(255)
#  customer_shipping_address2 :string(255)
#  customer_shipping_city     :string(255)
#  customer_shipping_province :string(255)
#  customer_shipping_zip      :string(255)
#  customer_shipping_country  :string(255)
#  created_at                 :datetime
#  updated_at                 :datetime
#

class Order < ActiveRecord::Base
  ShopifyAPI::Base.site = "https://3fd9effe5aef663e58dfd78a918e3ef2:741f4006631fd33228e87ef4fd18d69b@korduroy.myshopify.com/admin"
  
  belongs_to :maker
  has_many :order_items
  
  accepts_nested_attributes_for :order_items, :allow_destroy => true
  
  #default_scope :order => 'order_date DESC'
end
