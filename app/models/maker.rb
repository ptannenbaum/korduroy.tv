# == Schema Information
#
# Table name: makers
#
#  id                 :integer          not null, primary key
#  created_at         :datetime
#  updated_at         :datetime
#  name               :string(255)
#  title              :string(255)
#  bio                :text
#  bio_image          :string(255)
#  visible            :boolean
#  crew               :boolean
#  maker_category_id  :integer
#  slug               :string(255)
#  storefront         :boolean          default(FALSE)
#  collection_id      :integer
#  details            :text
#  external_blog_feed :string(255)
#  vimeo              :string(255)
#  youtube            :string(255)
#  other_url          :string(255)
#  allow_contact_form :boolean          default(TRUE)
#

class Maker < ActiveRecord::Base
  ShopifyAPI::Base.site = "https://3fd9effe5aef663e58dfd78a918e3ef2:741f4006631fd33228e87ef4fd18d69b@korduroy.myshopify.com/admin"
  
  validates_presence_of :name, :maker_category_id
  
  has_one     :user
  belongs_to  :maker_category
  has_many    :featured_makers
  has_many    :shows, :through => :featured_makers
  has_many    :maker_photos
  has_many    :maker_videos
  has_many    :maker_contacts
  has_many    :orders
  has_many    :order_items
  
  accepts_nested_attributes_for :maker_photos, :allow_destroy => true, :reject_if => :all_blank
  accepts_nested_attributes_for :maker_videos, :allow_destroy => true, :reject_if => :all_blank
  accepts_nested_attributes_for :orders, :allow_destroy => true, :reject_if => :all_blank
  accepts_nested_attributes_for :order_items, :allow_destroy => true, :reject_if => :all_blank
  accepts_nested_attributes_for :user, :allow_destroy => false
  
  attr_protected :crew
  
  validates_format_of :slug, :with => /\A[-a-z0-9]+\Z/i, :message => 'is invalid, only dashes (-) and alphanumeric charachters are allowed'
  
  mount_uploader :bio_image, BioImageUploader
  
  paginates_per 20
  
  before_save :create_shopify_collection
  after_save :hide_shopify_collection, :show_shopify_collection
  
  scope :makers_with_external_blogs, where('external_blog_feed is not null')
  
  def self.get_related_maker(vendor)
    self.where(:id => vendor).limit(1)
  end
  
  ### this is set in the blogs model
  #def self.maker_blog_posts
  #  @user = User.where(:default_blogger => false).where('maker_id >= 0')
  #  self.where(:user_id => @user, :visible => true)
  #end

private

  def create_shopify_collection
    if self.storefront & self.collection_id.blank?
      @collection = ShopifyAPI::CustomCollection.create(:title => self.name)
      self.collection_id = @collection.id
    end
  end
  
  def hide_shopify_collection
    if !self.storefront & !self.collection_id.blank?
      @c = ShopifyAPI::CustomCollection.find(self.collection_id)
      @c.update_attributes({"published" => false})
    end
  end
  
  def show_shopify_collection
    if self.storefront & self.collection_id
      @c = ShopifyAPI::CustomCollection.find(self.collection_id)
      @c.update_attributes({"published" => true})
    end
  end
  
  # maker collections should be deleted through the shopify admin
end
