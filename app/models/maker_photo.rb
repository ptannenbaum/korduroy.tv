# == Schema Information
#
# Table name: maker_photos
#
#  id              :integer          not null, primary key
#  title           :string(255)
#  description     :text
#  visible         :boolean          default(TRUE)
#  comments_closed :boolean
#  photo           :string(255)      not null
#  maker_id        :integer
#  created_at      :datetime
#  updated_at      :datetime
#

class MakerPhoto < ActiveRecord::Base
  belongs_to :maker
  
  mount_uploader :photo, MakerPhotoUploader
  
  def self.randumb(maker)
    self.where("maker_id = ?", maker.id).random
  end
end
