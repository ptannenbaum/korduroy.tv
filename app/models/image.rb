# == Schema Information
#
# Table name: images
#
#  id         :integer          not null, primary key
#  image      :string(255)      not null
#  created_at :datetime
#  updated_at :datetime
#  blog_id    :integer
#

class Image < ActiveRecord::Base
  belongs_to :blog
  
  attr_protected :blog_id
  
  mount_uploader :image, ImageUploader
  
  # Build a list to populate tinyMCE
  def self.image_list(images)
    images.map{ |image| [ image.image_identifier, image.image.url ] }
  end
end
