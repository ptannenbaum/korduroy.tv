# == Schema Information
#
# Table name: answers
#
#  id         :integer          not null, primary key
#  name       :string(255)
#  position   :integer
#  url        :string(255)
#  image      :string(255)
#  poll_id    :integer
#  created_at :datetime
#  updated_at :datetime
#

class Answer < ActiveRecord::Base
  belongs_to :poll
  
  acts_as_list :scope => :poll
  
  validates_presence_of :name
  
  mount_uploader :image, AnswerImageUploader
  
  default_scope order(:position)
end
