# == Schema Information
#
# Table name: campaign_banners
#
#  id         :integer          not null, primary key
#  post_image :string(255)
#  link       :string(255)
#  show_home  :boolean          default(FALSE)
#  created_at :datetime
#  updated_at :datetime
#

class CampaignBanner < ActiveRecord::Base
  validates_presence_of :post_image, :link

  mount_uploader :post_image, PostImage1Uploader
  paginates_per 10
end
