# == Schema Information
#
# Table name: supporters
#
#  id                    :integer          not null, primary key
#  name                  :string(255)
#  address               :string(255)
#  city                  :string(255)
#  state                 :string(255)
#  zip_code              :string(255)
#  telephone             :string(255)
#  website               :string(255)
#  email                 :string(255)
#  logo                  :string(255)
#  ad                    :string(255)
#  description           :text
#  created_at            :datetime
#  updated_at            :datetime
#  supporter_category_id :integer
#  display_order         :integer
#

class Supporter < ActiveRecord::Base
  validates_presence_of :name, :telephone, :website, :email, :description
  
  belongs_to  :supporter_category
  
  mount_uploader :ad,   AdUploader
  mount_uploader :logo, LogoUploader
  
  default_scope :order => 'display_order ASC'
end
