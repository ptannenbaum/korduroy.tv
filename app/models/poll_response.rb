# == Schema Information
#
# Table name: poll_responses
#
#  id         :integer          not null, primary key
#  poll_id    :integer          not null
#  user_id    :integer          not null
#  answer_id  :integer          not null
#  position   :integer          not null
#  created_at :datetime
#  updated_at :datetime
#

class PollResponse < ActiveRecord::Base
  belongs_to  :user
  belongs_to  :answer
  
  # need to validate that answers are unique in the context of the poll and user
  validates_uniqueness_of :answer_id, :scope => [:poll_id, :user_id]
  
  def self.save_votes user, poll, responses
    ActiveRecord::Base.transaction do
      return false if Poll.user_already_voted?(user, poll)
      
      responses.reverse!
      1.upto responses.count do |i|
        poll_response = PollResponse.new(
          :poll_id => poll.id, :user_id => user.id,
          :answer_id => responses[i-1], :position => i
        )
        poll_response.save!
      end
    end
    true
  end
end
