# == Schema Information
#
# Table name: order_items
#
#  id                      :integer          not null, primary key
#  order_id                :integer
#  maker_id                :integer
#  item_id                 :integer
#  item_product_id         :integer
#  item_name               :string(255)
#  item_qty                :integer
#  item_price              :string(255)
#  item_vendor             :string(255)
#  item_sku                :string(255)
#  item_variant_id         :integer
#  item_weight             :integer
#  item_fulfillment_status :string(255)
#  created_at              :datetime
#  updated_at              :datetime
#

class OrderItem < ActiveRecord::Base
  
  belongs_to :order
  belongs_to :maker
  
  after_create :send_email
  
  def send_email
    StoreNotification.new_purchase(self).deliver
  end
end
