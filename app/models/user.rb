# == Schema Information
#
# Table name: users
#
#  id                     :integer          not null, primary key
#  email                  :string(255)      default(""), not null
#  encrypted_password     :string(128)      default(""), not null
#  reset_password_token   :string(255)
#  reset_password_sent_at :datetime
#  remember_created_at    :datetime
#  sign_in_count          :integer          default(0)
#  current_sign_in_at     :datetime
#  last_sign_in_at        :datetime
#  current_sign_in_ip     :string(255)
#  last_sign_in_ip        :string(255)
#  created_at             :datetime
#  updated_at             :datetime
#  admin                  :boolean          default(FALSE)
#  supplier               :boolean          default(FALSE)
#  first_name             :string(255)
#  last_name              :string(255)
#  avatar                 :string(255)
#  default_blogger        :boolean          default(FALSE)
#  twitter                :string(255)
#  facebook               :string(255)
#  tumbler                :string(255)
#  wordpress              :string(255)
#  maker_id               :integer
#  authentication_token   :string(255)
#  flickr                 :string(255)
#  blogger                :string(255)
#  pinterest              :string(255)
#

class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :token_authenticatable, :encryptable, :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable, :recoverable, :rememberable, :trackable, :validatable, :token_authenticatable, :mailchimp

  # Setup accessible (or protected) attributes for your model
  attr_accessible :email, :password, :password_confirmation, :remember_me
  attr_protected  :admin, :maker_id, :supplier, :default_blogger
  
  has_many    :blogs, :order => "created_at DESC"
  belongs_to  :maker
  
  validates_presence_of :first_name, :last_name
  
  mount_uploader :avatar, AvatarUploader
  
  def name
    "#{first_name} #{last_name}"
  end
  
  def is_maker?
    maker_id.present?
  end
  
  # acts_as_rateable requires the model to respond to login
  def login
    name
  end
  
end
