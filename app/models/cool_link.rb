# == Schema Information
#
# Table name: cool_links
#
#  id               :integer          not null, primary key
#  name             :string(255)
#  url              :string(255)
#  created_at       :datetime
#  updated_at       :datetime
#  cool_category_id :integer
#

class CoolLink < ActiveRecord::Base
  validates_presence_of :name, :url, :cool_category_id
  
  belongs_to :cool_category
  
  default_scope :order => "name ASC"
end
