# == Schema Information
#
# Table name: featured_makers
#
#  id         :integer          not null, primary key
#  maker_id   :integer
#  show_id    :integer
#  created_at :datetime
#  updated_at :datetime
#

class FeaturedMaker < ActiveRecord::Base
  belongs_to :maker
  belongs_to :show
end
