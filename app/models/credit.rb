# == Schema Information
#
# Table name: credits
#
#  id             :integer          not null, primary key
#  show_id        :integer
#  credit_type_id :integer
#  name           :string(255)
#  url            :string(255)
#  created_at     :datetime
#  updated_at     :datetime
#

class Credit < ActiveRecord::Base
  validates_presence_of :credit_type_id, :name
  
  belongs_to :credit_type
  belongs_to :show
end
