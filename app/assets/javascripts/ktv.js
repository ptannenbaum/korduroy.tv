// Document Ready
$(function(){
	alinks();
	
	// help/info copy toggle
	$("#info-text").hide();
	$("#info-button a").click(function(e) {
		e.preventDefault();
		$(this).parent("p").toggleClass("active");
		$("#info-text").slideToggle();
	});
	$("#info-close").click(function(e) {
		e.preventDefault();
		$("#info-button").removeClass("active");
		$("#info-text:visible").slideUp();
	});
	
	//Turn all the select boxes into rating controls
    $(".ranking > .rating").rating({
		showCancel: false
	});
	$(".ranking > .rating").each(function() {
		$(this).bind("change", function(){
			var container = $(this).parent(".ranking");
			var show = $(this).data("show");
			var episode = $(this).data("episode");
			var rating = $(this).serialize();
			//alert(JSON.stringify(rating));
			
			container.block({
				css: {
					padding: 7,
					margin: -19,
					width: '100%',
					top: '0',
					left: '0',
					textAlign: 'center',
					color: '#000',
					border: '0',
					backgroundColor: '#fff',
					cursor: 'wait'
				},
				message: "Processing..."
			});
			
			var request = $.ajax({
				type: 'POST',
				url: "/shows/"+show+"/"+episode+"/rate",
				data: rating
			});
			
			request.done(function(data) {
				//alert("Success: " + data);
				$(this).prop("disabled", true);
				container.unblock();
			});

			request.fail(function(jqXHR, textStatus) {
		 		alert("Error: " + textStatus);
			});
		});
	});
	
	// sort list rankings
//	$(".sortable").not(".disabled").sortable({
//		forcePlaceholderSize: true,
//		placeholder: "ui-state-highlight",
//		opacity: 0.8,
//		revert: true,
//		tolerance: 'pointer',
//		handle: '.move'
//	});
//	$(".sortable").disableSelection();
//	$(".sortable li").each(function() {
//		$(this).hover(function() {
//			$(".listImg").removeClass("active");
//			$(this).find("img.listImg").addClass("active");
//		});
//	});
//	$(".results li").each(function() {
//		$(this).hover(function() {
//			$(this).siblings("li").removeClass("active");
//			$(this).addClass("active");
//			var pct = $(this).find(".pct").text();
//			$(this).parents("div").find(".pctActive").text(pct);
//			$(".listImg").removeClass("active");
//			$(this).find("img.listImg").addClass("active");
//		});
//	});
	
	// list accordion
	$("#listAccordion").accordion();
	
	// list tabs
	$(".tabbed").tabs({
		fx: {
			opacity: 'toggle',
			duration: "fast"
		}
	});
	
	// list voting
	$.blockUI.defaults.css = {};
	$.blockUI.defaults.overlayCSS = {};
	$(".pollButton").on("click", function(e) {
		var container = $(this).parents("article");
		container.block({
			message: '<h1 class="voteProcessing">Processing</h1>'
		});
		var poll = $(this).parent().attr("rel");
		var vote = [];
		var items = $(this).prev("ol").children("li");
		items.each(function() {
			vote.push(this.id);
		});
		// ajax call to controller method
		var request = $.ajax({
			type: 'POST',
			url: '/the-list/vote/'+poll,
			data: { 'votes[]': vote }
		});
		
		request.done(function(data) {
			container.find(".pollButton").fadeOut();
			container.find(".sortable").addClass("disabled");
			container.unblock();
			container.find(".resultsLink a").trigger("click");
		});
		
		request.fail(function(jqXHR, textStatus) {
			container.block({
				message: '<h1>EPIC FAIL</h1><p>Try voting again.</p>'
			});
			setTimeout($.unblockUI, 5000); 
		});
		
		//alert(vote);
	});
	
	// carousels
	var $relatedItems = $("#relatedItems");
	var $relatedList = $("#itemList");
	if ($relatedItems.length) {
		$relatedItems.append("<div id=\"related-carousel-prev\">Prev</div><div id=\"related-carousel-next\">Next</div>");

		$relatedList.carouFredSel({
			circular: false,
			infinite: false,
			height: "auto",
			auto : {
				items : 1,
				duration : 800,
				easing : "linear",
				pauseDuration : 0
			}
		}).trigger("pause");


		$("#related-carousel-prev").on({
			mouseover: function() {
				$relatedList.trigger("configuration", ["direction", "right"]);
				$relatedList.trigger("play");
			},
			mouseout: function() {
				$relatedList.trigger("pause");
			},
			click: function() {
				return false;
			}
		});

		$("#related-carousel-next").on({
			mouseover: function() {
				$relatedList.trigger("configuration", ["direction", "left"]);
				$relatedList.trigger("play");
			},
			mouseout: function() {
				$relatedList.trigger("pause");
			},
			click: function() {
				return false;
			}
		});
	}
	
	// fade image shadow on rollover
	$(".postImage, .inlinePostImage").hover(function() {
		$(this).animate({boxShadow: "0 2px 10px rgba(0, 0, 0, 0.4)"}, 200);
	}, function() {
		$(this).animate({boxShadow: "0 2px 10px rgba(102, 102, 102, 0.3)"}, 200);
	});
	
	// init shadowbox
	Shadowbox.init({
		skipSetup: true
	});
	
	// welcome message
	if ($("#status-message").length) {
		if (top == self) {
			top.location.href='/'; 
		}
		var t = setTimeout(shutit, 2000);
	}

    // newsletter signup
    $("#newsletter-form").bind('ajax:success', function() {
        $(this).html('<div id="newsletter-mahalo"><p>Your all signed up. Mahalo!</p></div>')
    });
});

// Document load
$(window).load(function() {
	
	if ($("#videoIframe").length) {
		// callVimeo();
	}
	
	Shadowbox.setup(".signUpLink, .signInLink, .myAccountLink, .sign-in, .sign-up", {
		width: 650,
		height: 550,
		overlayOpacity: 0.85
	});
	
	// vjustify related items
	if ($("#relatedItems").length) {
		$("#relatedList li").vjustify();
	}
	
	// fix blog, makers and product navigation on scroll
	if ($("#makersNavigation").length) {
		var mkrNavPos = $("#makersNavigation").offset();
		var mkrNavTop = mkrNavPos.top-25;
		$(window).scroll(function() {
		    if ($(this).scrollTop() > mkrNavTop) {
		        $("#makersNavigation").css({ "position": "fixed", "top": 25 });
		    } else {
		        $("#makersNavigation").css({ "position": "static" }); //same here
		    }                           
		});
	}
	
	if ($("#ktvBlogNavigation").length) {
		var blogNavPos = $("#ktvBlogNavigation").offset();
		var blogNavTop = blogNavPos.top-25;
		$(window).scroll(function() {
		    if ($(this).scrollTop() > blogNavTop) {
		        $("#ktvBlogNavigation").css({ "position": "fixed", "top": 25 });
		    } else {
		        $("#ktvBlogNavigation").css({ "position": "static" }); //same here
		    }                           
		});
	}
	
	if ($("#productNavigation").length) {
		var prodNavPos = $("#productNavigation").offset();
		var prodNavTop = prodNavPos.top-25;
		$(window).scroll(function() {
		    if ($(this).scrollTop() > prodNavTop) {
		        $("#productNavigation").css({ "position": "fixed", "top": 25 });
		    } else {
		        $("#productNavigation").css({ "position": "static" }); //same here
		    }                           
		});
	}
	
	if ($("#twitter").length) {
		var uname = $("#twitter").data("twitterhandle");
		$("#tweet").tweet({
			avatar_size: 48,
			count: 1,
			username: uname,
			loading_text: "loading tweet...",
			template: "{avatar}{user}{text}{time}"
		});
	}
	
	getCartTotal();
});

function alinks() {
	// open external links in new window
	$(".externalLink").each(function() {
		$(this).click(function(e) {
			e.preventDefault();
			window.open(this.href);
		});
	});
	
	var emailLink = $(".emailLink");
	emailLink.each(function(e) {
		$(this).click(function() {
			var to = this.id;
			var dom = this.rel;
			this.href = "mailto:"+to+"@"+dom;
		});
	});
	
	var sbCancelLink = $(".sbCancelLink");
	sbCancelLink.each(function() {
		$(this).click(function(e) {
			e.preventDefault();
			parent.Shadowbox.close();
		});
	});
}

function getCartTotal() {
	postMessage('cart', 'http://korduroy.myshopify.com')
}

function shutit() {
	parent.location.reload();
	parent.Shadowbox.close();
}

window.addEventListener("message", function (e) {
    if (e.origin !== 'http://korduroy.myshopify.com') {
		return;
	} else if (e.data == "checkout") {
		location.href = "/store";
	} else {
        console.log("data", e.data);
        if (e.data !== "0 Items : $0.00") {
            $("#cartLink a").text("Cart: " + e.data);
            $("#cartMsg").html("<h3>Cart: <span>" + e.data + "</span></h3>");
        }
	}
}, false);