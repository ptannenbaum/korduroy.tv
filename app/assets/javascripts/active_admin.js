//= require jquery
//= require jquery-ui
//= require jquery_ujs
//= require plupload/plupload.full

$(function() {
	$(".datepicker").datepicker({dateFormat: 'yy-mm-dd'});
	
	$(".clear_filters_btn").click(function() {
		window.location.search = "";
		return false;
	});

	// fix sidebar on scroll
	/*
	$(window).scroll(function() {
	    if ($(this).scrollTop() > 110) {
	        $("#active_admin_content #sidebar").css({ "position": "fixed", "top": 25 });
	    } else {
	        $("#active_admin_content #sidebar").css({ "position": "static" });
	    }                           
	});
	*/
});