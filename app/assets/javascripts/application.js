// This is a manifest file that'll be compiled into including all the files listed below.
// Add new JavaScript/Coffee code in separate files in this directory and they'll automatically
// be included in the compiled file accessible from http://example.com/assets/application.js
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// the compiled file.
//
//= require jquery
//= require jquery-ui
//= require jquery_ujs

//= require safe_logging
//= require file_uploader
//= require jquery.rating
//= require jquery.jscrollpane
//= require jquery.mousewheel
//= require jquery.jCarousel
//= require jquery.vjustify
//= require jquery.shadow_animation
//= require jquery.isotope
//= require jquery.infinite_scroll
//= require jquery.carouFredSel
//= require jquery.blockUI
//= require jquery.tweet
//= require jquery.cookie
//= require jquery.stringToSlug
//= require shadowbox/shadowbox
//= require plupload/plupload.full
//= require tiny_mce/jquery.tinymce

//= require ktv
//= require home
//= require makers
//= require shows
//= require stores
//= require slug
//= require makers_admin/blogs
//= require makers_admin/makers
