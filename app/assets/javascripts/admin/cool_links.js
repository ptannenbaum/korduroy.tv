$("#columns article").vjustify();

$("#columns").sortable({
	axis: "x",
	items: "article",
	handle: "h2",
	opacity: 0.7,
	forcePlaceholderSize: true,
	placeholder: "ui-sortable-placeholder",
	revert: false,
	tolerance: "pointer"
});
$("#columns").disableSelection();