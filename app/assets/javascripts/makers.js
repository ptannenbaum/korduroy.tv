// Isotope Filtering
if ($("#makers_controller").length) {
	$("#infinite-pagination .pagination").css("visibility", "hidden");
	
	var $container = $("#makersGallery #makersList");
	$container.imagesLoaded(function(){
		$container.isotope({
			itemSelector : '#makersList > li'
		});

		$container.infinitescroll({
			navSelector  : '.pagination',    // selector for the paged navigation 
			nextSelector : '.pagination .next a',  // selector for the NEXT link (to page 2)
			itemSelector : '#makersList > li',     // selector for all items you'll retrieve
			loading: {
				msgText: "<em>Loading more posts...</em>",
				finishedMsg: 'No more posts to load.',
				img: '/assets/loader_horizontal.gif'
			}
		}, function(newElements) {
			var $newElems = $(newElements);
			$newElems.imagesLoaded(function() {
				$container.isotope('insert', $(newElements));
			})
		});
	});
	

	var $list = $("#makersList.list");
	$list.imagesLoaded( function(){
		$list.isotope({
			layoutMode : 'straightDown',
			itemSelector : '#makersList.list > li'
		});

		$list.infinitescroll({
			navSelector  : '.pagination',    // selector for the paged navigation 
			nextSelector : '.pagination .next a',  // selector for the NEXT link (to page 2)
			itemSelector : '#makersList.list > li',     // selector for all items you'll retrieve
			loading: {
				msgText: "<em>Loading more makers...</em>",
				finishedMsg: 'No more makers to load.',
				img: '/assets/loader_horizontal.gif'
			}
		}, function(newElements) {
			$list.isotope('insert', $(newElements));
		});
	});
	

	// makers navigation
	$("#makersNavigation a").hover(function() {
		var match = this.rel;
		$("#makersList li" + match).addClass("active");
	}, function() {
		$("#makersList > li").removeClass("active");
	});

	$("#makersList > li").hover(function() {
		var match = $(this).attr("class");
		$("#makersNavigation a[rel=\""+match+"\"]").parent("li").toggleClass("active");
	});

	$('#makersNavigation a').click(function(e) {
		$("#makersNavigation li").removeClass("active");
		$(this).parent("li").addClass("active");
		if ($container.length) {
			var selector = $(this).attr('data-filter');
			$container.isotope({ filter: selector });
			return false;
		}
		if ($list.length) {
			var selector = $(this).attr('data-filter');
			$list.isotope({ filter: selector });
			return false;
		}
	});
	
	$("#makersList .categoryLink").click(function(e) {
		var cat = "." + $(this).data("category");
		$("#makersNavigation li").removeClass("active");
		$("#makersNavigation li."+cat).addClass("active");
		$list.isotope({ filter: cat });
		return false;
	});
	
	
		var color = "cfff66";

		// vimeo & youtube resize
		$(".postBody iframe").each(function() {
		    var $obj = $(this);
		    var data = $obj.attr("data");
		    var vsrc = $obj.attr("src");
		    var oldW = $obj.attr("width");
			var newW = $("#blogDetail").length ? 650 : 515;
		    var oldH = $obj.attr("height");
		    var p = oldW/newW;
		    var newH = (oldH/p);
			if (vsrc) {
				if (vsrc.search("vimeo") > 0) {
		    	    $obj.replaceWith("<iframe src=\""+vsrc+"?title=0&byline=0&portrait=0&color="+color+"\" width=\""+newW+"\" height=\""+newH+"\" frameborder=\"0\"></iframe>");
			    }
			    if (vsrc.search("youtube") > 0) {
			        $obj.replaceWith("<iframe src=\""+vsrc+"\" width=\""+newW+"\" height=\""+newH+"\" frameborder=\"0\"></iframe>");
			    }
			} else {
				$obj.remove();
			}
		});

		// vimeo & youtube object resize
		$(".postBody object").each(function() {
		    var $obj = $(this);
		    var $emb = $obj.children("embed");
		    var vsrc = $emb.attr("src");
		    var oldW = $obj.attr("width");
		    var newW = $("#blogDetail").length ? 650 : 515;
		    var oldH = $obj.attr("height");
		    var p = oldW/newW;
		    var newH = (oldH / p);
		    $obj.attr("width", newW);
		    $obj.attr("height", newH);
		    $emb.attr("width", newW);
		    $emb.attr("height", newH);
		});

		// get rid of 1x1 images in external blog feeds
	
		$(".postBody img").each(function() {
			var t = $(this);
			if ((t.width() == 1) || (t.height() == 1)) {
				t.remove();
			}
		})


	if ($("#makerGallery").length) {
		Shadowbox.setup(".makerVideoLink", {
			gallery: "My Videos",
			width: 855,
			height: 480,
			overlayOpacity: 0.85
		});

		Shadowbox.setup(".makerPhotoLink", {
			gallery: "My Images",
			overlayOpacity: 0.85
		});
		
		$(window).load(function() {
			$("#makerVideos li").vjustify();
		});
	}
	
	$(".postBody a").click(function(e) {
		e.preventDefault();
		window.open(this.href);
	});
	
	$("#largeImage").not(".single").stop().carouFredSel({
		circular : false,
		infinite : false,
		auto : false,
		items : 1,
		height : "auto",
		align : "center",
		scroll : {
			fx : "crossfade"
		},
		direction : "down",
		pagination : {
			container : "#productThumbnails",
			anchorBuilder : function( nr, item ) {
				var src = $(item).attr( "src" );
				src = src.replace("_large", "_compact");
				return '<a href="#" class="item' +nr+ '"><img src="' + src + '"></a>'
			},
			onBefore: function() {
				var pos = $(this).triggerHandler( "currentPosition" );
				$("a").removeClass( "active" );
				$("a.item"+(pos+1)).addClass( "active" );
			}
		}
	});
	
	$("#productThumbnails").stop().carouFredSel({
		circular : false,
		infinite : false,
		auto : false,
		align: "center",
		scroll : {
			items : 1
		},
		direction: "up",
		prev : "#scnPrev",
		next : "#scnNext"
	});
	
	$(".show-carousel").not(".disabled").append("<div class=\"show-carousel-prev\">Prev</div><div class=\"show-carousel-next\">Next</div>");

	$(".show-carousel:not(.disabled) ul").carouFredSel({
		circular: false,
		infinite: false,
		height: "auto",
		auto : {
			items : 1,
			duration : 800,
			easing : "linear",
			pauseDuration : 0
		}
	}).trigger("pause");


	$(".show-carousel-prev").on({
		mouseover: function() {
			$(this).parent(".show-carousel").find("ul").trigger("configuration", ["direction", "right"]);
			$(this).parent(".show-carousel").find("ul").trigger("play");
		},
		mouseout: function() {
			$(this).parent(".show-carousel").find("ul").trigger("pause");
		},
		click: function() {
			return false;
		}
	});

	$(".show-carousel-next").on({
		mouseover: function() {
			$(this).parent(".show-carousel").find("ul").trigger("configuration", ["direction", "left"]);
			$(this).parent(".show-carousel").find("ul").trigger("play");
		},
		mouseout: function() {
			$(this).parent(".show-carousel").find("ul").trigger("pause");
		},
		click: function() {
			return false;
		}
	});
	
	$("#socialNetworks a").not(".add, .edit, .cancel").click(function(e) {
		e.preventDefault();
		window.open(this.href);
	});
}
