if ($("#shows_controller").length) {
	// shows carousels
	$(".show-carousel").not(".disabled").append("<div class=\"show-carousel-prev\">Prev</div><div class=\"show-carousel-next\">Next</div>");

	$(".show-carousel:not(.disabled) ul").carouFredSel({
		circular: false,
		infinite: false,
		height: "auto",
		auto : {
			items : 1,
			duration : 800,
			easing : "linear",
			pauseDuration : 0
		}
	}).trigger("pause");
	
	$(".show-carousel-prev").on({
		mouseover: function() {
			$(this).parent(".show-carousel").find("ul").trigger("configuration", ["direction", "right"]);
			$(this).parent(".show-carousel").find("ul").trigger("play");
		},
		mouseout: function() {
			$(this).parent(".show-carousel").find("ul").trigger("pause");
		},
		click: function() {
			return false;
		}
	});

	$(".show-carousel-next").on({
		mouseover: function() {
			$(this).parent(".show-carousel").find("ul").trigger("configuration", ["direction", "left"]);
			$(this).parent(".show-carousel").find("ul").trigger("play");
		},
		mouseout: function() {
			$(this).parent(".show-carousel").find("ul").trigger("pause");
		},
		click: function() {
			return false;
		}
	});

	$(".show-carousel.disabled li").vjustify();
	
	// show hero previews
	$("#shows_controller #hero.video aside li:first").addClass("active");
	
	
	// shows landing page
	// add scrollbars to video player
	$("#hero.showsMain aside ul").jScrollPane({
		showArrows: true,
		verticalArrowPositions: 'split',
		mouseWheelSpeed: 10,
		clickOnTrack: true,
		autoReinitialise: true
	});

	$("#hero.showsMain aside ul").bind("jsp-arrow-change", function(event, isAtTop, isAtBottom, isAtLeft, isAtRight) {
		if (isAtTop) {
			//alert("top");
		}

		if (isAtBottom) {
			//alert('bottom');
		}

	});
	
	var $showWidth = $("#hero.showsMain").length ? 720 : 960;
	var $showHeight = $("#vimeoEmbed").attr("height");
	
	$('#hero aside a').each(function() {
		$(this).click(function(e) {
			e.preventDefault();
			
			$('body,html').animate({
				scrollTop: 0
			}, 800);
			
			var show = this.href;
			var slug = $(this).data("slug");
			var vid = $(this).data("video");
			var heroHeight = $("#hero").height();

			if ($(this).parent("li").hasClass("active")) {
				return false;
			} else {
				$(this).parents("ul").find("li").removeClass("active");
				$(this).parent("li").addClass("active");
				$("#hero figure").animate({
					opacity: 0
				}, 800, function() {
					// get vimeo embed
					$.getJSON("http://vimeo.com/api/oembed.json?url=http%3A//vimeo.com/" + vid + "&width=" + $showWidth + "&callback=?", function(data) {
						$showHeight = data['height'];
					});
					$(this).load(show + "?preview=true #hero figure", function() {
						$("#hero figure > figure").unwrap();
						$("iframe").attr({
							"width": $showWidth,
							"height": $showHeight
						});
						$(".rating").rating({showCancel: false});
						$(".ranking > .rating").each(function() {
							$(this).bind("change", function(){
								var container = $(this).parent(".ranking");
								var show = $(this).data("show");
								var episode = $(this).data("episode");
								var rating = $(this).serialize();
								//alert(JSON.stringify(rating));

								container.block({
									css: {
										padding: 7,
										margin: -19,
										width: '100%',
										top: '0',
										left: '0',
										textAlign: 'center',
										color: '#000',
										border: '0',
										backgroundColor: '#fff',
										cursor: 'wait'
									},
									message: "Processing..."
								});

								var request = $.ajax({
									type: 'POST',
									url: "/shows/"+show+"/"+episode+"/rate",
									data: rating
								});

								request.done(function(data) {
									//alert("Success: " + data);
									$(this).prop("disabled", true);
									container.unblock();
								});

								request.fail(function(jqXHR, textStatus) {
							 		alert("Error: " + textStatus);
								});
							});
						});
						$("#hero figure").animate({
							opacity: 1
						}, 800);
					});	
				});
				// callVimeo();
			}
		})
	});
}


