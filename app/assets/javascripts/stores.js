if ($("#stores_controller").length) {
	$("#hero .productList li").vjustify();
	
	if ($("#productGallery").length) {
		var $container = $("#productGallery");
		$container.imagesLoaded( function(){
			$container.isotope({
				itemSelector : '#productGallery > li',
				layoutStyle : 'fitRows'
			});
			
			$container.infinitescroll({
				navSelector  : '.pagination',    // selector for the paged navigation 
				nextSelector : '.pagination .next a',  // selector for the NEXT link (to page 2)
				itemSelector : '#productGallery > li',     // selector for all items you'll retrieve
				loading: {
					msgText: "<em>Loading more products...</em>",
					finishedMsg: 'No more products to load.',
					img: '/assets/loader_horizontal.gif'
				},
				extraScrollPx: 300,
			}, function(newElements) {
				var $newElems = $(newElements);
				$newElems.imagesLoaded(function() {
					$container.isotope('insert', $(newElements));
				});
			});
		});
	}
	
	$('#productNavigation a').click(function(e) {
		$("#productNavigation li").removeClass("active");
		$(this).parent("li").addClass("active");
		var selector = $(this).attr('data-filter');
		var selText = $(this).text();
		$("#products > h2").text(selText);
		$container.isotope({ filter: selector });
		return false;
	});
	
	$(".productList li").hover(function() {
		$(this).find(".thumb.store").animate({
			opacity: 0.35
		}, 300)
	}, function() {
		$(this).find(".thumb.store").animate({
			opacity: 1
		}, 300)
	});
	
	$("#largeImage").not(".single").stop().carouFredSel({
		circular : false,
		infinite : false,
		auto : false,
		items : 1,
		height : "auto",
		align : "center",
		scroll : {
			fx : "crossfade"
		},
		direction : "down",
		pagination : {
			container : "#productThumbnails",
			anchorBuilder : function( nr, item ) {
				var src = $(item).attr( "src" );
				src = src.replace("_large", "_compact");
				return '<a href="#" class="item' +nr+ '"><img src="' + src + '"></a>'
			},
			onBefore: function() {
				var pos = $(this).triggerHandler( "currentPosition" );
				$("a").removeClass( "active" );
				$("a.item"+(pos+1)).addClass( "active" );
			}
		}
	});
	
	$("#productThumbnails").stop().carouFredSel({
		circular : false,
		infinite : false,
		auto : false,
		height: "auto",
		align: "center",
		scroll : {
			items : 1
		},
		prev : "#scnPrev",
		next : "#scnNext"
	});
	
	$(".show-carousel").not(".disabled").append("<div class=\"show-carousel-prev\">Prev</div><div class=\"show-carousel-next\">Next</div>");

	$(".show-carousel:not(.disabled) ul").carouFredSel({
		circular: false,
		infinite: false,
		height: "auto",
		auto : {
			items : 1,
			duration : 800,
			easing : "linear",
			pauseDuration : 0
		}
	}).trigger("pause");


	$(".show-carousel-prev").on({
		mouseover: function() {
			$(this).parent(".show-carousel").find("ul").trigger("configuration", ["direction", "right"]);
			$(this).parent(".show-carousel").find("ul").trigger("play");
		},
		mouseout: function() {
			$(this).parent(".show-carousel").find("ul").trigger("pause");
		},
		click: function() {
			return false;
		}
	});

	$(".show-carousel-next").on({
		mouseover: function() {
			$(this).parent(".show-carousel").find("ul").trigger("configuration", ["direction", "left"]);
			$(this).parent(".show-carousel").find("ul").trigger("play");
		},
		mouseout: function() {
			$(this).parent(".show-carousel").find("ul").trigger("pause");
		},
		click: function() {
			return false;
		}
	});

}

if (($("#stores_controller").length) || $("#makerProduct").length) {
	checkStock();
	// variant select menus
	var $opt1 = $("#option1");
	var $opt2 = $("#option2");
	var $opt3 = $("#option3");
	var $addbtn = $("#add-to-cart");
	var $prange = $("#price").text();
	
	if ($opt2.length) $opt2.children("option").not(":first").prop("disabled", true);
	if ($opt3.length) $opt3.children("option").not(":first").prop("disabled", true);
	
	$opt1.change(function() {
		if ($opt2.length) {
			$opt2.val("-1").children("option").not(":first").prop("disabled", true);
			$("#price").text($prange);
		}
		if ($opt3.length) {
			$opt3.val("-1").children("option").not(":first").prop("disabled", true);
			$("#price").text($prange);
		}
	});
	
	$opt2.change(function() {
		if ($opt3.length) {
			$opt3.val("-1").children("option").not(":first").prop("disabled", true);
			$("#price").text($prange);
		}
	})
	
	$("#variants select").change(function() {
		// get selected value
		var sel = $(this).val();
		
		$("#variantValues input:hidden").each(function() {
			var do1 = $(this).data("option1");
			var do2 = $(this).data("option2");
			var do3 = $(this).data("option3");
			var ok  = $(this).data("available");
			
			if (do1 == sel) {
				
				if (do2 != "") {
					//alert(do2);
					$opt2.children("option").each(function() {
						if ($(this).val() == do2) {
							$(this).prop("disabled", false);
						}
					});
				} else {
					var p = $(this).data("price").toFixed(2);
					if (ok == "yes") {
						$("#price").text("$"+p);
						$addbtn.animate({opacity: 1}, 300).prop("disabled", false);
					} else {
						$("#price").text("SOLD OUT")
						$addbtn.prop("disabled", true);
					}
				}
			}
			
			if (do2 == sel) {	
				if (do3 != "") {
					$opt3.children("option").each(function() {
						if ($(this).val() == do3) {
							$(this).prop("disabled", false);
						}
					});
				} else {
					if (do1 == $opt1.val()) {
						var p = $(this).data("price").toFixed(2);
						if (ok == "yes") {
							$("#price").text("$"+p);
							$addbtn.animate({opacity: 1}, 300).prop("disabled", false);
						} else {
							$("#price").text("SOLD OUT")
							$addbtn.prop("disabled", true);
						}
						
					}
				}
			}
			
			if (do3 == sel) {
				if ((do1 == $opt1.val()) && (do2 == $opt2.val())) {
					var p = $(this).data("price").toFixed(2);
					if (ok == "yes") {
						$("#price").text("$"+p);
						$addbtn.animate({opacity: 1}, 300).prop("disabled", false);
					} else {
						$("#price").text("SOLD OUT")
						$addbtn.prop("disabled", true);
					}
				}
				
			}
		});
	});

	$("#add-to-cart").click(function(e) {
		if ($("#vnum").val() > 1) {
			var sel1 = $("#option1 :selected").val();
			var sel2 = $("#option2 :selected").val();
			var sel3 = $("#option3 :selected").val();
			//alert(sel1 + " : " + sel2 + " : " + sel3);

			if ((sel1 == "-1") || (sel2 == "-1") || (sel3 == "-1")) {
				alert('Please make sure all options have a valid selection.');
				return false;
			} else {
				$("#variantValues input:hidden").each(function() {
					if (($(this).data("option1") != sel1) && ($(this).data("option1") != "")) {
						$(this).remove();
					}
					if (($(this).data("option2") != sel2) && ($(this).data("option2") != "")) {
						$(this).remove();
					}
					if (($(this).data("option3") != sel3) && ($(this).data("option3") != "")) {
						$(this).remove();
					}
				});
			}
		}
	});
}

function checkStock() {
	var ok  = $("#variantValues input:hidden").first().data("available");
	var price = $("#price");
	var btn = $("#add-to-cart");
	
	// check product stock
	if (ok == "no") {
		price.text("SOLD OUT")
		btn.animate({opacity: 0.5}, 100).prop("disabled", true);
	}
}