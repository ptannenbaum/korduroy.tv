$(document).ready(function() {
  load_maker_editors();
  tagTabs();
});

function load_maker_editors() {
	$('#maker_bio').tinymce({
		script_url : "/javascripts/tiny_mce/tiny_mce.js",
		theme: "advanced",
		plugins: "autolink, lists, advimage, paste, media",
		paste_strip_class_attributes : true,
		paste_remove_spans : true,
		paste_remove_styles : true,
		past_text_sticky : true,
		paste_text_linebreaktype : "p",
		theme_advanced_buttons1: "formatselect,|,bold,italic,underline,|,bullist,numlist,outdent,indent,|,pastetext,undo,redo,|,link,unlink,image,media,vimeo,|,cleanup,removeformat,code",
		theme_advanced_buttons2 : "",
		theme_advanced_buttons3 : "",
		theme_advanced_buttons4 : "",
		theme_advanced_toolbar_location: "top",
		theme_advanced_toolbar_align: "left",
		theme_advanced_statusbar_location: "bottom",
		theme_advanced_resizing: true
	});
	
	$('#maker_details').tinymce({
		script_url : "/javascripts/tiny_mce/tiny_mce.js",
		theme: "advanced",
		plugins: "autolink, lists, advimage, paste, media, vimeo",
		paste_strip_class_attributes : true,
		paste_remove_spans : true,
		paste_remove_styles : true,
		past_text_sticky : true,
		paste_text_linebreaktype : "p",
		theme_advanced_buttons1: "formatselect,|,bold,italic,underline,|,bullist,numlist,outdent,indent,|,pastetext,undo,redo,|,link,unlink,image,media,vimeo,|,cleanup,removeformat,code",
		theme_advanced_buttons2 : "",
		theme_advanced_buttons3 : "",
		theme_advanced_buttons4 : "",
		theme_advanced_toolbar_location: "top",
		theme_advanced_toolbar_align: "left",
		theme_advanced_statusbar_location: "bottom",
		theme_advanced_resizing: true
	});
}

// tag accordion
function tagAccordion() {
	$("#tagAccordion").accordion();
}

// tag tabs
function tagTabs() {
	$("#tagTabs").tabs();
}

// makers admin functions
if ($("#productForm").length) {
	// add new inputs
	$(".create_new_input").hide();
	$(".custom_option").change(function() {
		var cval = $(this).val();
		if (cval == "create_new") {
			$(this).siblings(".create_new_input").attr("value", "").show();
		} else {
			$(this).siblings(".create_new_input").attr("value", cval).hide();
		}
	});

	// tags
	var input = $("#tags");
	$(".tagList a").click(function(e) {
		e.preventDefault();
		var text = $(this).text();
		var curtext = input.val();
		if ($(this).hasClass("active-tag")) {
			$(this).removeClass("active-tag");
			var tlength = text.length;
			if (curtext.substr(0,tlength+2) == text + ", ") {
				newtext = curtext.replace(text + ", ", "");
			} else if (curtext == text) {
				newtext = curtext.replace(text, "");
			} else {
				newtext = curtext.replace(", " + text, "");
			}
			input.val(newtext);
		} else {
			$(this).addClass("active-tag");
			input.val((curtext ? curtext + ", " : "") + text);
		}
	});
	
	// images
	$(".imagesList.sortable").sortable({
		forcePlaceholderSize: true,
		placeholder: "ui-state-highlight",
		opacity: 0.8,
		revert: true,
		tolerance: 'pointer',
		handle: '.move',
		update: function(event, ui) {
			$(".imagesList li").each(function() {})
		}
	});
	
	// validate new product submission fields
	if ($("#product-image-fields").length) {
		$("#productForm input:submit").click(function(e) {
			if ($("#title").val() == "") {
				alert('Please enter a title for this product');
				$("#title").focus();
				return false;
			}
			if ($("#product_image_product_image").val() == "") {
				alert('Please select a product image to upload');
				$("#product_image_product_image").focus();
				return false;
			}
		});
	}
	
	$("#productImagesForm input:submit").click(function() {
		if ($("#productImagesForm input:file").val() == "") {
			alert('Please select a file to upload.');
			return false;
		}
	});
	
	// delete product images
	var deleteImage = $(".imagesList .delete");
	deleteImage.each(function() {
		$(this).click(function(e) {
			e.preventDefault();
			var ximg = $(this).parent("li");
			if (confirm("Are you sure you want to delete this record?")) {
				var request = $.ajax({
					type: "DELETE",
					url: this.href
				});
				
				request.done(function(data) {
					ximg.fadeOut();
				});

				request.fail(function(jqXHR, textStatus) {
					alert(textStatus); 
				});
			}
		});
	});
	
	// variants table & edit/update functions
	$("#variantsTable .editable").hide();
	
	$("#variantsTable .edit").click(function(e) {
		e.preventDefault();
		$("#newVariantForm:visible").slideUp();
		var $ebtn = $(this);
		$ebtn.animate({
			opacity: 0
		}, 300)
		var vdata = $ebtn.data("vid");
		$("#v-"+vdata).slideDown();
	});
	
	$("#variantsTable .cancel").on("click", function(e) {
		e.preventDefault();
		var $cbtn = $(this);
		$(this).parents(".editable").slideUp();
		$cbtn.parents("tr").prev("tr").find(".edit").animate({
			opacity: 1
		}, 300)
	});
	
	$(".adminForm .add:not(.disabled)").click(function(e) {
		e.preventDefault();
		$("#variantsTable tbody .editable:visible").slideUp();
		var $abtn = $(this);
		$abtn.addClass("disabled");
		$("#newVariantForm").slideDown();
	});
	
	$(".button.close").click(function(e) {
		e.preventDefault();
		$("#newVariantForm:visible").slideUp();
		$("#variantsTable .add.disabled").removeClass("disabled");
	});
	
	// delete variant
	var deleteVariant = $("#variantsTable .actions-cell .delete");
	deleteVariant.each(function() {
		$(this).click(function(e) {
			e.preventDefault();
			var xvar = $(this).data("vid");
			if (confirm("Are you sure you want to delete this record?")) {
				var request = $.ajax({
					type: "DELETE",
					url: this.href
				});
				
				request.done(function(data) {
					$(".tr-"+xvar).fadeOut();
				});

				request.fail(function(jqXHR, textStatus) {
					alert(textStatus); 
				});
			}
		});
	});
	
	//  Options
	if ($("#productOptions").length) {
		var $optButton = $("#addOptionButton");
		var $optRemove = $("#productOptions .actions .cancel");
		var $opt1Fields = $("#option1-fields");
		var $opt2Fields = $("#option2-fields");
		var $opt3Fields = $("#option3-fields");
		var $step = 1;

		$optRemove.hide();
		$opt2Fields.hide().find("#option_selector_2").children("option").prop("disabled", true);
		$opt3Fields.hide().find("#option_selector_3").children("option").prop("disabled", true);

		$optButton.on("click", function(e) {
			e.preventDefault();
			if ($(this).hasClass("disabled")) {
				return;
			} else {
				if ($step == 1) {

					if (($("#option_selector_1 :selected").val() == "Title") || ($("#option_value_1").val() == "")) {
						alert("Please select and enter values for the first option before creating another.")
					} else {
						$opt2Fields.slideDown();
						$optRemove.fadeIn();

						var s1 = $("#option_selector_1 :selected").val();
						$("#option_selector_2 option").each(function() {
							if ($(this).text() != s1) {
								$(this).prop("disabled", false);
							}
						});
						$step = 2;
						return;
					}
				} else if ($step == 2) {

					if ( ($("#option_selector_2").val() == "Title") || ($("#option_value_2").val() == "") ) {
						alert("Please select and enter values for the second option before creating another.");
					} else {
						$opt3Fields.slideDown();
						var s1 = $("#option_selector_1 :selected").val();
						var s2 = $("#option_selector_2 :selected").val();
						$("#option_selector_3 option").each(function() {
							if (($(this).text() != s2) && ($(this).text() != s1)) {
								$(this).prop("disabled", false);
							}
						});
						$step = 3;
						$optButton.animate({ opacity: 0.5 }, 300).addClass("disabled");
						return;
					}
				}
			}
		});
	
		$optRemove.on("click", function(e) {
			e.preventDefault();
			switch ($step) {
				case 1:
					break;
			
				case 2:
					$("#option_selector_2").val("Title");
					$("#option_value_2").val("");
					$opt2Fields.slideUp();
					$step = 1;
					$optRemove.animate({ opacity: 0 }, 300);
					break;
			
				case 3:
					$("#option_selector_3").val("Title");
					$("#option_value_3").val("");
					$opt3Fields.slideUp();
					$step = 2;
					$optButton.animate({ opacity: 1 }, 300).removeClass("disabled");
					break;
			}
		});
	}
	
	var $trackQTY = $("#track_inventory");
	var $qtyInput = $("#inventory_quantity");
	
	$trackQTY.click(function() {
		if ($trackQTY.prop("checked")) {
			$qtyInput.removeClass("disabled").prop("disabled", false).val("1");
			$qtyInput.parent("div").addClass("required");
		} else {
			$qtyInput.addClass("disabled").val("").prop("disabled", true);
			$qtyInput.parent("div").removeClass("required");
		}
	});
	
}

if ($("#editProductOptions").length) {
	
	var $optButton = $("#addOptionButton");
	var $optCancel = $("#editProductOptions .cancel");
	var $opt1Fields = $("#option1-fields");
	var $opt2Fields = $("#option2-fields");
	var $opt3Fields = $("#option3-fields");
	var $optDelete  = $("#editProductOptions .delete");
	var $step = $("#editProductOptions").data("step");
	
	switch ($step) {
		case 1:
			$opt2Fields.hide().find("#option_selector_2").children("option").prop("disabled", true);
			$opt3Fields.hide().find("#option_selector_3").children("option").prop("disabled", true);
			break;
		
		case 2:
			var s1 = $("#option_selector_1 :selected").val();
			$("#option_selector_2 option").each(function() {
				if ($(this).text() == s1) {
					$(this).prop("disabled", true);
				}
			});
			$opt3Fields.hide().find("#option_selector_3").children("option").prop("disabled", true);
			break;
		
		case 3:
			var s1 = $("#option_selector_1 :selected").val();
			var s2 = $("#option_selector_2 :selected").val();
			var s3 = $("#option_selector_3 :selected").val();
			$("#option_selector_1 option").each(function() {
				if (($(this).text() == s2) || ($(this).text() == s3)) {
					$(this).prop("disabled", true);
				}
			});
			$("#option_selector_2 option").each(function() {
				if (($(this).text() == s1) || ($(this).text() == s3)) {
					$(this).prop("disabled", true);
				}
			});
			$("#option_selector_3 option").each(function() {
				if (($(this).text() == s1) || ($(this).text() == s2)) {
					$(this).prop("disabled", true);
				}
			});
			break;
	}

	$optButton.bind("click", function(e) {
		e.preventDefault();
		if ($(this).hasClass("disabled")) {
			return;
		} else {
			if ($step == 1) {

				if (($("#option_selector_1 :selected").val() == "Title") || ($("#option_value_1").val() == "") || ($("#option_value_1").val() == "Default Title")) {
					alert("Please select and enter values for the first option before creating another.")
				} else {
					$opt2Fields.slideDown();
					$opt2Fields.find(".cancel").animate({ opacity: 1 }, 300);

					var s1 = $("#option_selector_1 :selected").val();
					$("#option_selector_2 option").each(function() {
						if ($(this).text() != s1) {
							$(this).prop("disabled", false);
						}
					});
					$step = 2;
					return;
				}
			} else if ($step == 2) {

				if ( ($("#option_selector_2").val() == "Title") || ($("#option_value_2").val() == "") ) {
					alert("Please select and enter values for the second option before creating another.");
				} else {
					$opt3Fields.slideDown();
					$opt2Fields.find(".cancel").animate({ opacity: 0 }, 300);
					var s1 = $("#option_selector_1 :selected").val();
					var s2 = $("#option_selector_2 :selected").val();
					$("#option_selector_3 option").each(function() {
						if (($(this).text() != s2) && ($(this).text() != s1)) {
							$(this).prop("disabled", false);
						}
					});
					$step = 3;
					$optButton.animate({ opacity: 0.5 }, 300).addClass("disabled");
					return;
				}
			}
		}
	});

	$optCancel.bind("click", function(e) {
		e.preventDefault();
		switch ($step) {
			case 1:
				break;

			case 2:
				$("#option_selector_2").val("Title");
				$("#option_value_2").val("");
				$opt2Fields.slideUp();
				$step = 1;
				break;

			case 3:
				$("#option_selector_3").val("Title");
				$("#option_value_3").val("");
				$opt3Fields.slideUp();
				$step = 2;
				$opt2Fields.find(".cancel").animate({ opacity: 1 }, 300);
				$optButton.animate({ opacity: 1 }, 300).removeClass("disabled");
				break;
		}
	});
	
	$optDelete.bind("click", function(e) {
		e.preventDefault();
		var xopt = $(this).parent("div").parent("div");
		xopt.slideUp(300, function() {
			xopt.remove();
			$("#productOptionForm").submit();
		});
	});
}

if ($("#variantsTable").length) {
	$(function() {
		var $trackQTY = $('.track_qty');
		var $qtyInput = $('.qty');
		$trackQTY.each(function() {
			var pv = $(this).data("pv");
			var savedQTY = $("#inventory_quantity_"+pv).val();
			$(this).on("click", function() {
				if ($(this).prop("checked")) {
					$qtyInput.removeClass("disabled").prop("disabled", false).val(""+savedQTY+"");
					$qtyInput.parent("div").addClass("required");
				} else {
					$qtyInput.addClass("disabled").val("").prop("disabled", true);
					$qtyInput.parent("div").removeClass("required");
				}
			});
		});
	});
}

if ($("#makers_admin-blogs_controller").length) {
	// tags
	var input = $("#blog_tags");
	$(".tagList a").click(function(e) {
		e.preventDefault();
		var text = $(this).text();
		var curtext = input.text();
		if ($(this).hasClass("active-tag")) {
			$(this).removeClass("active-tag");
			$("#blog_tag_list_" + text).remove();
			var tlength = text.length;
			if (curtext.substr(0,tlength+2) == text + ", ") {
				newtext = curtext.replace(text + ", ", "");
			} else if (curtext == text) {
				newtext = curtext.replace(text, "");
			} else {
				newtext = curtext.replace(", " + text, "");
			}
			input.text(newtext);
		} else {
			$(this).addClass("active-tag");
			$("#tagsInput").append("<input type=\"hidden\" id=\"blog_tag_list_" + text + "\" name=\"blog[tag_list][]\" value=\"" + text + "\">")
			input.text((curtext ? curtext + ", " : "") + text);
		}
	});
	
	// delete blog images
	var deleteImage = $(".imagesList .delete");
	deleteImage.each(function() {
		$(this).click(function(e) {
			e.preventDefault();
			var ximg = $(this).parent("li");
			if (confirm("Are you sure you want to delete this record?")) {
				var request = $.ajax({
					type: "DELETE",
					url: this.href
				});
				
				request.done(function(data) {
					ximg.fadeOut();
				});

				request.fail(function(jqXHR, textStatus) {
					alert(textStatus); 
				});
			}
		});
	});
}

if ($(".maker-admin").length) {
	var $addLink = $("#socialNetworks .add");
	var $editLink = $("#socialNetworks .edit");
	
	$addLink.toggle(function(e) {
		e.preventDefault();
		$(this).text("Cancel");
		$(this).parent("li").next(".add-network-item").slideDown(300);
	}, function() {
		var txt = $(this).data("add");
		$(this).text(txt);
		$(this).parent("li").next(".add-network-item").slideUp(300);
	});
	
	$editLink.toggle(function(e) {
		e.preventDefault();
		$(this).text("Cancel");
		$(this).parent("li").next(".add-network-item").slideDown(300);
	}, function() {
		$(this).text("Edit");
		$(this).parent("li").next(".add-network-item").slideUp(300);
	})
}

if ($("#fulfillmentForm").length) {
	var call = $("#checkAll");
	var track_no = $("#tracking_number");
	var cboxes = $("#fulfillmentTable input:checkbox");
	
	call.click(function() {
		if ($(this).prop("checked") == true) {
			cboxes.prop("checked", true);
		} else {
			cboxes.prop("checked", false);
		}
	});
	cboxes.each(function() {
		$(this).click(function() {
			if ($(this).prop("checked") == false) {
				call.prop("checked", false);
			}
		});
	});
	
	$("#fulfillmentForm").submit(function() {
		if (track_no.val() == "") {
			alert('please enter a tracking number');
			return false;
		}
	});
}