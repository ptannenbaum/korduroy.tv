$(document).ready(function() {
  load_editors()
});

function load_editors() {
	$('#blog_content').tinymce({
		script_url : "/javascripts/tiny_mce/tiny_mce.js",
		theme: "advanced",
		plugins: "autolink, lists, paste, media, vimeo",
		relative_urls : false,
		remove_script_host : false,
		paste_strip_class_attributes : true,
		paste_remove_spans : true,
		paste_remove_styles : true,
		past_text_sticky : true,
		paste_text_linebreaktype : "p",
		theme_advanced_buttons1: "formatselect,|,bold,italic,underline,|,bullist,numlist,outdent,indent,|,pastetext,undo,redo,|,link,unlink,image,media,vimeo,|,cleanup,removeformat,code",
		theme_advanced_buttons2 : "",
		theme_advanced_buttons3 : "",
		theme_advanced_buttons4 : "",
		theme_advanced_toolbar_location: "top",
		theme_advanced_toolbar_align: "left",
		theme_advanced_statusbar_location: "bottom",
		theme_advanced_resizing: true
	});
	
	$('#body_html').tinymce({
		script_url : "/javascripts/tiny_mce/tiny_mce.js",
		theme: "advanced",
		plugins: "autolink, lists, paste",
		paste_strip_class_attributes : true,
		paste_remove_spans : true,
		paste_remove_styles : true,
		past_text_sticky : true,
		paste_text_linebreaktype : "p",
		theme_advanced_buttons1: "formatselect,|,bold,italic,underline,|,bullist,numlist,outdent,indent,|,pastetext,undo,redo,|,link,unlink|,cleanup,removeformat,code",
		theme_advanced_buttons2 : "",
		theme_advanced_buttons3 : "",
		theme_advanced_buttons4 : "",
		theme_advanced_toolbar_location: "top",
		theme_advanced_toolbar_align: "left",
		theme_advanced_statusbar_location: "bottom",
		theme_advanced_resizing: true
	});
	
	/*$("form").submit(function() {
		$('#blog_content, #body_html').tinymce().save();
	});*/
}