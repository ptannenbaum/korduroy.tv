if ($("#home_controller").length) {
    $(window).load(function () {
        $("#hs").carouFredSel({
            circular: true,
            infinite: true,
            auto: {
                pauseDuration: 10000,
                pauseOnHover: true,
            },
            height: "auto",
            items: {
                visible: 1,
                height: "auto"
            },
            align: "center",
            onCreate: function(items) {
                items.find(".featured-copy:hidden").delay(2000).slideToggle(600);
                $("#home-scroller").find(".panel").each(function(){
                   $(this).css("visibility","visible");
                });
                $("#hsPrev").css("visibility","visible");
                $("#hsNext").css("visibility","visible");
            },
            scroll: {
                fx: "directscroll",
                items: "page",
                onBefore: function(oldItems, newItems, newSizes, duration) {
                    oldItems.find(".featured-copy").clearQueue().stop().hide();
                },
                onAfter: function(oldItems, newItems, newSizes) {
                    newItems.find(".featured-copy").delay(2000).slideToggle(600);
                }
            },
            prev: {
                button: "#hsPrev",
                onBefore: function(oldItems, newItems, newSizes, duration) {
                    oldItems.find(".featured-copy").clearQueue().stop().hide();
                },
                onAfter: function(oldItems, newItems, newSizes) {
                    newItems.find(".featured-copy").delay(2000).slideToggle(600);
                }
            },
            next: {
                button: "#hsNext",
                onBefore: function(oldItems, newItems, newSizes, duration) {
                    oldItems.find(".featured-copy").clearQueue().stop().hide();
                },
                onAfter: function(oldItems, newItems, newSizes) {
                    newItems.find(".featured-copy").delay(2000).slideToggle(600);
                }
            },
            direction: "left",
            pagination: "#hs-pag"
        });
    });
}