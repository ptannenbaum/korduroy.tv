KorduroyTv::Application.routes.draw do
  
  ActiveAdmin.routes(self)

  # home routes
  root :to => "home#index"
  match 'home/add_newsletter'                       => 'home#add_newsletter'

  # blog routes
  get 'blog'                                        => 'blogs#index'
  get 'blog/rss'                                    => 'blogs#rss',              :as => 'blog_rss'
  get 'blog/:year/:slug'                            => 'blogs#show',             :year => /\d{4}/, :slug => /[-a-zA-Z0-9]+/, :as => 'main_blog'
  post 'blog/:year/:slug/comment'                   => 'blogs#comment',          :year => /\d{4}/, :slug => /[-a-zA-Z0-9]+/
  get 'blog/:year/:slug/comment'                    => 'blogs#show',             :year => /\d{4}/, :slug => /[-a-zA-Z0-9]+/
  get 'blog/:slug'                                  => 'blogs#category',         :slug => /[-a-zA-Z0-9]+/
  
  # shows routes
  resources :show_categories do
    resources :shows
  end
  get 'shows'                                       => 'shows#index'
  get 'shows/:slug'                                 => 'shows#category',         :slug => /[-a-zA-Z0-9]+/
  get 'shows/:slug/:show_slug'                      => 'shows#show',             :slug => /[-a-zA-Z0-9]+/, :show_slug => /[-a-zA-Z0-9]+/
  post 'shows/:slug/:show_slug/comment'             => 'shows#comment',          :slug => /[-a-zA-Z0-9]+/, :show_slug => /[-a-zA-Z0-9]+/
  get 'shows/:slug/:show_slug/comment'              => 'shows#show',             :slug => /[-a-zA-Z0-9]+/, :show_slug => /[-a-zA-Z0-9]+/
  post 'shows/:slug/:show_slug/rate'                => 'shows#rate',             :slug => /[-a-zA-Z0-9]+/, :show_slug => /[-a-zA-Z0-9]+/
  
  # makers routes
  get 'makers'                                      => 'makers#index'
  get 'makers/posts'                                => 'makers#index'
  get 'makers/list'                                 => 'makers#list'
  get 'makers/:slug'                                => 'makers#show',            :slug => /[-a-zA-Z0-9]+/,  :as => 'maker'
  get 'makers/:slug/blog/:year/:post_slug/:id'      => 'makers#blog',            :year => /\d{4}/, :slug => /[-a-zA-Z0-9]+/, :post_slug => /[-a-zA-Z0-9]+/, :id   => /[0-9]+/, :as => 'makers_blog'
  post 'makers/:slug/blog/:year/:post_slug/:id/comment' => 'makers#comment',     :year => /\d{4}/, :slug => /[-a-zA-Z0-9]+/, :post_slug => /[-a-zA-Z0-9]+/, :id   => /[0-9]+/
  get 'makers/:slug/blog/:year/:post_slug/:id/comment'  => 'makers#blog',        :year => /\d{4}/, :slug => /[-a-zA-Z0-9]+/, :post_slug => /[-a-zA-Z0-9]+/, :id   => /[0-9]+/
  get 'makers/:slug/rss'                            => 'makers#rss',             :slug => /[-a-zA-Z0-9]+/, :as => 'makers_rss'
  get 'makers/:slug/store'                          => 'makers#store',           :slug => /[-a-zA-Z0-9]+/
  get 'makers/:slug/store/:product'                 => 'makers#product',         :slug => /[-a-zA-Z0-9]+/, :product => /[0-9]+/
  get 'makers/:slug/gallery'                        => 'makers#gallery',         :slug => /[-a-zA-Z0-9]+/
  get 'makers/:slug/bio'                            => 'makers#bio',             :slug => /[-a-zA-Z0-9]+/
  get 'makers/category/:slug'                       => 'makers#category',        :slug => /[-a-zA-Z0-9]+/
  get 'makers/:slug/contact'                        => 'maker_contacts#new',     :slug => /[-a-zA-Z0-9]+/
  post 'makers/:slug/contact'                       => 'maker_contacts#create',  :slug => /[-a-zA-Z0-9]+/
  get 'makers/:slug/contact/thanks'                 => 'maker_contacts#thanks',  :slug => /[-a-zA-Z0-9]+/
  
  # the list routes
  resources :poll_categories do
    resources :polls
  end
  get 'the-list'                                    => 'polls#index'
  get 'the-list/:id'                                => 'polls#show',             :id   => /[0-9]+/
  post 'the-list/vote/:id'                          => 'polls#vote',             :id   => /[0-9]+/
  get 'the-list/:slug'                              => 'polls#category',         :slug => /[-a-zA-Z0-9]+/
  get 'the-list/results/:id'                        => 'polls#results',          :id   => /[0-9]+/
  post 'the-list/:id/comment'                       => 'polls#comment',          :id   => /[0-9]+/
  get 'the-list/:id/comment'                        => 'polls#show',             :id   => /[0-9]+/
  
  #store routes
  get 'store'                                       => 'stores#index'
  get 'store/:id'                                   => 'stores#show',            :id => /[0-9]+/
  get 'store/cart'                                  => 'stores#cart'

  #supporters routes
  resources :supporters, :only => [:index, :show]
  
  #cool routes
  get 'links'                                       => 'cool#index'
  
  #about routes
  get 'about'                                       => 'pages#about'
  get 'about/projects'                              => 'pages#projects'
  get 'about/big-shaka-club'                        => 'pages#club'
  get 'privacy'                                     => 'pages#privacy'
  get 'terms'                                       => 'pages#terms'
  get 'submissions'                                 => 'pages#submissions'
  get 'returns'                                     => 'pages#returns'
  get 'help'                                        => 'pages#help'
  get 'reef_x_cyrus_collab'                         => 'pages#reef_x_cyrus'
  get 'compassing'                                  => 'pages#compassing'

  #contact routes
  get 'contact'                                     => 'contacts#new'
  post 'contact'                                    => 'contacts#create'
  get 'contact/thanks'                              => 'contacts#thanks'

  #users routes
  devise_for :users do
    get 'users', :to => 'home#status', :as => :user_root
  end
  get 'status'                                      => 'home#status'
  
  #utilities routes
  get 'utilities/load_external_feeds'               => 'utilities#load_external_feeds', :constraints => {:ip => /127.0.0.1/}
  
  #makers admin : stores
  get    'makers_admin/stores'                      => 'makers_admin/stores#index'
  get    'makers_admin/stores/new'                  => 'makers_admin/stores#new'
  post   'makers_admin/stores/new_product'          => 'makers_admin/stores#new_product'
  get    'makers_admin/stores/:id/edit'             => 'makers_admin/stores#edit', :id => /[0-9]+/
  post   'makers_admin/stores/:id/update'           => 'makers_admin/stores#update', :id => /[0-9]+/
  post   'makers_admin/stores/:id/update_options'   => 'makers_admin/stores#update_options', :id => /[0-9]+/
  post   'makers_admin/stores/:id/variant/new'      => 'makers_admin/stores#new_variant', :id => /[0-9]+/
  post   'makers_admin/stores/:product_id/images'   => 'makers_admin/stores#images', :id => /[0-9]+/
  post   'makers_admin/stores/:id/variant/:variant_id/update' => 'makers_admin/stores#update_variant', :id => /[0-9]+/, :variant_id => /[0-9]+/
  delete 'makers_admin/stores/:id'                  => 'makers_admin/stores#delete', :id => /[0-9]+/
  delete 'makers_admin/stores/:product_id/images/:image_id' => 'makers_admin/stores#delete_image', :product_id => /[0-9]+/, :image_id   => /[0-9]+/
  delete 'makers_admin/stores/:product_id/variant/:variant_id' => 'makers_admin/stores#delete_variant', :product_id => /[0-9]+/, :variant_id   => /[0-9]+/
  get    'makers_admin/stores/orders'               => 'makers_admin/stores#orders'
  get    'makers_admin/stores/order/:id'            => 'makers_admin/stores#order', :id => /[0-9]+/
  
  #makers admin : images
  
  #makers admin : others
  namespace :makers_admin do
    resources :maker_photos, :maker_videos, :makers, :products
    resources :blogs do
      get 'reblog', :on => :member
      resources :images
    end
  end

  # search routes
  get     'search'                                  => 'search#results',  :as => 'search'

  # orders routes
  match 'orders/new'                                => 'orders#new'
  match 'orders/create'                             => 'orders#update'
  post 'orders/fulfill'                             => 'orders#fulfill'

end
