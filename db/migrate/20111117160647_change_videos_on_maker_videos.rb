

class ChangeVideosOnMakerVideos < ActiveRecord::Migration
  def change
    rename_column :maker_videos, :video, :vimeo
    add_column    :maker_videos, :youtube, :string
  end
end
