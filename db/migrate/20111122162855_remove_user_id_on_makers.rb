

class RemoveUserIdOnMakers < ActiveRecord::Migration
  def up
    remove_column :makers, :user_id
  end

  def down
    add_column :makers, :user_id, :integer
  end
end
