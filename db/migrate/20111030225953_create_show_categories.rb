

class CreateShowCategories < ActiveRecord::Migration
  def change
    create_table :show_categories do |t|
      t.string :title
      t.text :description
      t.string :icon
      t.string :banner
      t.string :slug

      t.timestamps
    end
  end
end
