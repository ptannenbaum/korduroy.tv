

class AddFieldsToShows < ActiveRecord::Migration
  def change
    add_column :shows, :vimeo, :string
    add_column :shows, :youtube, :string
    add_column :shows, :thumbnail, :string
    add_column :shows, :show_category_id, :integer
    add_column :shows, :title, :string
    add_column :shows, :description, :text
    add_column :shows, :comments_closed, :boolean
    add_column :shows, :staff_pick, :boolean
  end
end
