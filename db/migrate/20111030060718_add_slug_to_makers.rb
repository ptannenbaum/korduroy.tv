

class AddSlugToMakers < ActiveRecord::Migration
  def change
    add_column :makers, :slug, :string
  end
end
