

class CreateOrderItems < ActiveRecord::Migration
  def change
    create_table :order_items do |t|
      t.integer :order_id
      t.integer :maker_id
      
      t.integer   :item_id
      t.integer   :item_product_id
      t.string    :item_name
      t.integer   :item_qty
      t.string    :item_price
      t.string    :item_vendor
      t.string    :item_sku
      t.integer   :item_variant_id
      t.integer   :item_weight
      t.string    :item_fulfillment_status

      t.timestamps
    end
  end
end
