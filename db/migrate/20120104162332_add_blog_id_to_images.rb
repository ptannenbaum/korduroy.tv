

class AddBlogIdToImages < ActiveRecord::Migration
  def change
    add_column :images, :blog_id, :integer
  end
end
