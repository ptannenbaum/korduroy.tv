

class AddCoolCategoryToCoolLink < ActiveRecord::Migration
  def change
    remove_column :cool_links, :category
    add_column :cool_links, :cool_category_id, :integer
  end
end
