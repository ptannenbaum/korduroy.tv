

class AddCommentsCountToShows < ActiveRecord::Migration
  def change
    add_column :shows, :comments_count, :integer
  end
end
