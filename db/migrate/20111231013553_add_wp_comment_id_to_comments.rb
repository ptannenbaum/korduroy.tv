

class AddWpCommentIdToComments < ActiveRecord::Migration
  def change
    add_column :comments, :wp_comment_id, :integer
  end
end
