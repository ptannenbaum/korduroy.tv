

class AddDisplayOrderToMakerCategories < ActiveRecord::Migration
  def change
    add_column :maker_categories, :display_order, :integer
  end
end
