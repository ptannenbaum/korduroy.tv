

class RemoveMakerOnUser < ActiveRecord::Migration
  def up
    remove_column :users, :maker
  end

  def down
    add_column :users, :maker, :boolean
  end
end
