

class CreateFeaturedMakers < ActiveRecord::Migration
  def change
    create_table :featured_makers do |t|
      t.integer :maker_id
      t.integer :show_id

      t.timestamps
    end
  end
end
