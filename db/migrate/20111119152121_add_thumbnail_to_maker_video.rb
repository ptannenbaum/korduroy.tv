

class AddThumbnailToMakerVideo < ActiveRecord::Migration
  def change
    add_column :maker_videos, :thumbnail, :string
  end
end
