

class AddSocialFieldsToUsers < ActiveRecord::Migration
  def change
    add_column :users, :twitter, :string
    add_column :users, :facebook, :string
    add_column :users, :tumbler, :string
    add_column :users, :wordpress, :string
  end
end
