

class AddUserIdToMakers < ActiveRecord::Migration
  def change
    add_column :makers, :user_id, :integer
  end
end
