

class AddDisplayOrderToSupporters < ActiveRecord::Migration
  def change
    add_column :supporters, :display_order, :integer
  end
end
