class CreateCampaignBanner < ActiveRecord::Migration
  def up
  	create_table :campaign_banners do |t|
	  t.string :post_image
	  t.string :link
	  t.boolean :show_home, :default => false

      t.timestamps
    end
  end

  def down
  end
end
