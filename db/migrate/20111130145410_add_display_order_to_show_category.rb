

class AddDisplayOrderToShowCategory < ActiveRecord::Migration
  def change
    add_column :show_categories, :display_order, :integer
  end
end
