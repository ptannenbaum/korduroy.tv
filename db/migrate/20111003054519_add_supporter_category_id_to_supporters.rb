

class AddSupporterCategoryIdToSupporters < ActiveRecord::Migration
  def up
    remove_column :supporters, :category
    add_column    :supporters, :supporter_category_id, :integer
  end
  
  def down
    add_column    :supporters, :category, :string
    remove_column :supporters, :supporter_category_id
  end
end
