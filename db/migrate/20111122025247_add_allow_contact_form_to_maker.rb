

class AddAllowContactFormToMaker < ActiveRecord::Migration
  def change
    add_column :makers, :allow_contact_form, :boolean, :default => true
  end
end
