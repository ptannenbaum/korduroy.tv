

class AddStoreBooleanToMaker < ActiveRecord::Migration
  def change
    add_column :makers, :storefront, :boolean, :default => false
    add_column :makers, :collection_id, :integer
  end
end
