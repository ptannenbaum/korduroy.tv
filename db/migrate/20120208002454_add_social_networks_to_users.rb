

class AddSocialNetworksToUsers < ActiveRecord::Migration
  def change
    add_column :users, :flickr, :string
    add_column :users, :blogger, :string
    add_column :users, :pinterest, :string
  end
end
