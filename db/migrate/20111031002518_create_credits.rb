

class CreateCredits < ActiveRecord::Migration
  def change
    create_table :credits do |t|
      t.integer :show_id
      t.integer :credit_type_id
      t.string :name
      t.string :url

      t.timestamps
    end
  end
end
