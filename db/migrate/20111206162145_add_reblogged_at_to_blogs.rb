

class AddRebloggedAtToBlogs < ActiveRecord::Migration
  def change
    add_column :blogs, :reblogged_at, :datetime
  end
end
