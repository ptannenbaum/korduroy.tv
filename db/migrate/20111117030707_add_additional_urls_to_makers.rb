

class AddAdditionalUrlsToMakers < ActiveRecord::Migration
  def change
    add_column :makers, :vimeo, :string
    add_column :makers, :youtube, :string
    add_column :makers, :other_url, :string
  end
end
