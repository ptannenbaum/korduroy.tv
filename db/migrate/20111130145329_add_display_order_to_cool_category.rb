

class AddDisplayOrderToCoolCategory < ActiveRecord::Migration
  def change
    add_column :cool_categories, :display_order, :integer
  end
end
