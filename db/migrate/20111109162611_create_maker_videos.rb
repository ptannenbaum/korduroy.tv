

class CreateMakerVideos < ActiveRecord::Migration
  def change
    create_table :maker_videos do |t|
      t.string :title
      t.text :description
      t.boolean :visible, :default => true
      t.boolean :comments_closed
      t.string :video, :null => false
      t.integer :maker_id

      t.timestamps
    end
  end
end
