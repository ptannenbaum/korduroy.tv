

class CreateMakerCategories < ActiveRecord::Migration
  def change
    create_table :maker_categories do |t|
      t.string :name
      t.string :slug

      t.timestamps
    end
  end
end
