

class CreateMakerPhotos < ActiveRecord::Migration
  def change
    create_table :maker_photos do |t|
      t.string :title
      t.text :description
      t.boolean :visible, :default => true
      t.boolean :comments_closed
      t.string :photo, :null => false
      t.integer :maker_id

      t.timestamps
    end
  end
end
