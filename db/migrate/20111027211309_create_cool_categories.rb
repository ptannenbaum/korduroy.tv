

class CreateCoolCategories < ActiveRecord::Migration
  def change
    create_table :cool_categories do |t|
      t.string :name
      t.string :slug

      t.timestamps
    end
  end
end
