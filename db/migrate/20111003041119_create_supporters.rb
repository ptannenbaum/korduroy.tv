

class CreateSupporters < ActiveRecord::Migration
  def change
    create_table :supporters do |t|
      t.string :name
      t.string :address
      t.string :city
      t.string :state
      t.string :zip_code
      t.string :telephone
      t.string :website
      t.string :email
      t.string :category
      t.string :logo
      t.string :ad
      t.text :description

      t.timestamps
    end
  end
end
