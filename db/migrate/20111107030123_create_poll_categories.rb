

class CreatePollCategories < ActiveRecord::Migration
  def change
    create_table :poll_categories do |t|
      t.string :name
      t.string :slug
      
      t.timestamps
    end
  end
end
