

class AddExternalBlogFeedToMaker < ActiveRecord::Migration
  def change
    add_column :makers, :external_blog_feed, :string
  end
end
