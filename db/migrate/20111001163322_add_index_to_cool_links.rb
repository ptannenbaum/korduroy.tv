

class AddIndexToCoolLinks < ActiveRecord::Migration
  def change
    add_index :cool_links, :category
  end
end
