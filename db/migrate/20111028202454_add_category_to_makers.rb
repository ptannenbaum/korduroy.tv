

class AddCategoryToMakers < ActiveRecord::Migration
  def change
    add_column :makers, :maker_category_id, :integer
  end
end
