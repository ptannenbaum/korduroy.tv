

class CreateCoolLinks < ActiveRecord::Migration
  def change
    create_table :cool_links do |t|
      t.string :name
      t.string :url
      t.integer :category_id

      t.timestamps
    end
  end
end
