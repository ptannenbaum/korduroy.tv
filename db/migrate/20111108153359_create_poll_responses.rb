

class CreatePollResponses < ActiveRecord::Migration
  def change
    create_table :poll_responses do |t|
      t.integer :poll_id,   :null => false
      t.integer :user_id,   :null => false
      t.integer :answer_id, :null => false
      t.integer :position,  :null => false

      t.timestamps
    end
  end
end
