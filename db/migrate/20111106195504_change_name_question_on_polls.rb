

class ChangeNameQuestionOnPolls < ActiveRecord::Migration
  def change
    rename_column :polls, :name, :question
    rename_table  :questions, :answers
  end
end
