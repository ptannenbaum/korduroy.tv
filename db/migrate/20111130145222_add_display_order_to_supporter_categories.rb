

class AddDisplayOrderToSupporterCategories < ActiveRecord::Migration
  def change
    add_column :supporter_categories, :display_order, :integer
  end
end
