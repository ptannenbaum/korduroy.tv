class AddFeatured < ActiveRecord::Migration
  def up
  		add_column :blogs, :featured, :boolean, :default => false
  		add_column :shows, :featured, :boolean, :default => false
  end

  def down
  		remove_column :blogs, :featured
  		remove_column :shows, :featured
  end
end
