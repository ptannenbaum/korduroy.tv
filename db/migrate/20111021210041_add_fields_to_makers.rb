

class AddFieldsToMakers < ActiveRecord::Migration
  def change
    add_column :makers, :name, :string
    add_column :makers, :title, :string
    add_column :makers, :bio, :text
    add_column :makers, :bio_image, :string
    add_column :makers, :visible, :boolean
    add_column :makers, :crew, :boolean
  end
end
