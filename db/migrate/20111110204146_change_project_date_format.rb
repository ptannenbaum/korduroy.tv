

class ChangeProjectDateFormat < ActiveRecord::Migration
  def change
    remove_column :projects, :start_date
    remove_column :projects, :end_date
    add_column :projects, :start_date, :date
    add_column :projects, :end_date, :date
  end
end
