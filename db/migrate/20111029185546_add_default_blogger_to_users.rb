

class AddDefaultBloggerToUsers < ActiveRecord::Migration
  def change
    add_column :users, :default_blogger, :boolean, :default => false
  end
end
