

class CreateSupporterCategories < ActiveRecord::Migration
  def change
    create_table :supporter_categories do |t|
      t.string :name
      t.string :slug

      t.timestamps
    end
  end
end
