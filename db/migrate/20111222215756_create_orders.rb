

class CreateOrders < ActiveRecord::Migration
  def change
    create_table :orders do |t|
      t.integer   :order_id
      t.integer   :order_number
      t.string    :order_name
      t.string    :order_financial_status
      t.string    :order_fulfillment_status
      t.text      :order_note
      t.datetime  :order_date
      t.datetime  :order_cancel_date
      t.string    :order_cancel_reason
      t.datetime  :order_close_date
      
      t.string    :customer_email
      
      t.string    :customer_billing_name
      t.string    :customer_billing_phone
      
      t.string    :customer_shipping_name
      t.string    :customer_shipping_email
      t.string    :customer_shipping_phone
      t.string    :customer_shipping_address1
      t.string    :customer_shipping_address2
      t.string    :customer_shipping_city
      t.string    :customer_shipping_province
      t.string    :customer_shipping_zip
      t.string    :customer_shipping_country
      
      t.timestamps
    end
  end
end
