

class AddCommentsCountToBlogs < ActiveRecord::Migration
  def change
    add_column :blogs, :comments_count, :integer
  end
end
