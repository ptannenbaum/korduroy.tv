

class AddMakerIdToUsers < ActiveRecord::Migration
  def up
    add_column :users, :maker_id, :integer
    
    # Must call this method so that the new column gets picked up
    User.reset_column_information
    
    Maker.all.each do |maker|
      user = User.find maker.user_id
      user.update_attributes!(:maker_id => maker.id)
    end
  end
  
  def down
    remove_column :users, :maker_id
  end
end
