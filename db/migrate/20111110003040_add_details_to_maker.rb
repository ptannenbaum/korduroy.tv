

class AddDetailsToMaker < ActiveRecord::Migration
  def change
    add_column :makers, :details, :text
  end
end
