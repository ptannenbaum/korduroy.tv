

class CreateBlogs < ActiveRecord::Migration
  def change
    create_table :blogs do |t|
      t.string  :title
      t.text    :content
      #t.string  :category
      t.string  :slug
      t.boolean :visible,         :default  => true
      t.boolean :comments_closed
      t.integer :user_id

      t.timestamps
    end
  end
end
