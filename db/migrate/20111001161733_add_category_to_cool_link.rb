

class AddCategoryToCoolLink < ActiveRecord::Migration
  def change
    add_column    :cool_links, :category, :string
    remove_column :cool_links, :category_id
  end
end
