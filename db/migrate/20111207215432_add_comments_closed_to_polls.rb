

class AddCommentsClosedToPolls < ActiveRecord::Migration
  def change
    add_column :polls, :comments_closed, :boolean
  end
end
