

class AddImpressionsToShows < ActiveRecord::Migration
  def change
    add_column :shows, :impressions, :integer, :default => 0
  end
end
