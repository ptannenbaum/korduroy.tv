

class AddCommentsCountToPolls < ActiveRecord::Migration
  def change
    add_column :polls, :comments_count, :integer
  end
end
