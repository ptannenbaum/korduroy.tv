

class AddDisplayOrderToBlogCategories < ActiveRecord::Migration
  def change
    add_column :blog_categories, :display_order, :integer
  end
end
