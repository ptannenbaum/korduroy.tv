

class CreateMakerContacts < ActiveRecord::Migration
  def change
    create_table :maker_contacts do |t|
      t.string :name,       :null => false
      t.string :email,      :null => false
      t.text :message,      :null => false
      t.integer :maker_id,  :null => false
      t.string :ip,         :null => false

      t.timestamps
    end
  end
end
