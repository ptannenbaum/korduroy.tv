

class CreateProductImages < ActiveRecord::Migration
  def change
    create_table :product_images do |t|
      t.string  :product_image,   :null => false
      t.integer :product_id,        :null => false

      t.timestamps
    end
  end
end
