

class CreateQuestions < ActiveRecord::Migration
  def change
    create_table :questions do |t|
      t.string :name
      t.integer :position
      t.string :url
      t.string :image
      t.integer :poll_id

      t.timestamps
    end
  end
end
