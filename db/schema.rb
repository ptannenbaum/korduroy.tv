# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20121127055432) do

  create_table "active_admin_comments", :force => true do |t|
    t.integer  "resource_id",   :null => false
    t.string   "resource_type", :null => false
    t.integer  "author_id"
    t.string   "author_type"
    t.text     "body"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "namespace"
  end

  add_index "active_admin_comments", ["author_type", "author_id"], :name => "index_active_admin_comments_on_author_type_and_author_id"
  add_index "active_admin_comments", ["namespace"], :name => "index_active_admin_comments_on_namespace"
  add_index "active_admin_comments", ["resource_type", "resource_id"], :name => "index_admin_notes_on_resource_type_and_resource_id"

  create_table "admin_users", :force => true do |t|
    t.string   "email",                                 :default => "", :null => false
    t.string   "encrypted_password",     :limit => 128, :default => "", :null => false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",                         :default => 0
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "admin_users", ["email"], :name => "index_admin_users_on_email", :unique => true
  add_index "admin_users", ["reset_password_token"], :name => "index_admin_users_on_reset_password_token", :unique => true

  create_table "answers", :force => true do |t|
    t.string   "name"
    t.integer  "position"
    t.string   "url"
    t.string   "image"
    t.integer  "poll_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "blog_categories", :force => true do |t|
    t.string   "name"
    t.string   "slug"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "display_order"
  end

  create_table "blogs", :force => true do |t|
    t.string   "title"
    t.text     "content"
    t.string   "slug"
    t.boolean  "visible",          :default => true
    t.boolean  "comments_closed"
    t.integer  "user_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "blog_category_id"
    t.string   "post_image"
    t.string   "url"
    t.integer  "comments_count"
    t.datetime "reblogged_at"
    t.integer  "wp_post_id"
    t.boolean  "featured",         :default => false
  end

  create_table "campaign_banners", :force => true do |t|
    t.string   "post_image"
    t.string   "link"
    t.boolean  "show_home",  :default => false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "comments", :force => true do |t|
    t.string   "title",            :limit => 50, :default => ""
    t.text     "comment"
    t.integer  "commentable_id"
    t.string   "commentable_type"
    t.integer  "user_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "wp_comment_id"
  end

  add_index "comments", ["commentable_id"], :name => "index_comments_on_commentable_id"
  add_index "comments", ["commentable_type"], :name => "index_comments_on_commentable_type"
  add_index "comments", ["user_id"], :name => "index_comments_on_user_id"

  create_table "contacts", :force => true do |t|
    t.string   "name",       :null => false
    t.string   "email",      :null => false
    t.text     "message",    :null => false
    t.string   "ip",         :null => false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "cool_categories", :force => true do |t|
    t.string   "name"
    t.string   "slug"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "display_order"
  end

  create_table "cool_links", :force => true do |t|
    t.string   "name"
    t.string   "url"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "cool_category_id"
  end

  create_table "credit_types", :force => true do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "credits", :force => true do |t|
    t.integer  "show_id"
    t.integer  "credit_type_id"
    t.string   "name"
    t.string   "url"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "featured_makers", :force => true do |t|
    t.integer  "maker_id"
    t.integer  "show_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "images", :force => true do |t|
    t.string   "image",      :null => false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "blog_id"
  end

  create_table "maker_categories", :force => true do |t|
    t.string   "name"
    t.string   "slug"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "display_order"
  end

  create_table "maker_contacts", :force => true do |t|
    t.string   "name",       :null => false
    t.string   "email",      :null => false
    t.text     "message",    :null => false
    t.integer  "maker_id",   :null => false
    t.string   "ip",         :null => false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "maker_photos", :force => true do |t|
    t.string   "title"
    t.text     "description"
    t.boolean  "visible",         :default => true
    t.boolean  "comments_closed"
    t.string   "photo",                             :null => false
    t.integer  "maker_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "maker_videos", :force => true do |t|
    t.string   "title"
    t.text     "description"
    t.boolean  "visible",         :default => true
    t.boolean  "comments_closed"
    t.string   "vimeo",                             :null => false
    t.integer  "maker_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "youtube"
    t.string   "thumbnail"
  end

  create_table "makers", :force => true do |t|
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "name"
    t.string   "title"
    t.text     "bio"
    t.string   "bio_image"
    t.boolean  "visible"
    t.boolean  "crew"
    t.integer  "maker_category_id"
    t.string   "slug"
    t.boolean  "storefront",         :default => false
    t.integer  "collection_id"
    t.text     "details"
    t.string   "external_blog_feed"
    t.string   "vimeo"
    t.string   "youtube"
    t.string   "other_url"
    t.boolean  "allow_contact_form", :default => true
  end

  create_table "order_items", :force => true do |t|
    t.integer  "order_id"
    t.integer  "maker_id"
    t.integer  "item_id"
    t.integer  "item_product_id"
    t.string   "item_name"
    t.integer  "item_qty"
    t.string   "item_price"
    t.string   "item_vendor"
    t.string   "item_sku"
    t.integer  "item_variant_id"
    t.integer  "item_weight"
    t.string   "item_fulfillment_status"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "orders", :force => true do |t|
    t.integer  "order_id"
    t.integer  "order_number"
    t.string   "order_name"
    t.string   "order_financial_status"
    t.string   "order_fulfillment_status"
    t.text     "order_note"
    t.datetime "order_date"
    t.datetime "order_cancel_date"
    t.string   "order_cancel_reason"
    t.datetime "order_close_date"
    t.string   "customer_email"
    t.string   "customer_billing_name"
    t.string   "customer_billing_phone"
    t.string   "customer_shipping_name"
    t.string   "customer_shipping_email"
    t.string   "customer_shipping_phone"
    t.string   "customer_shipping_address1"
    t.string   "customer_shipping_address2"
    t.string   "customer_shipping_city"
    t.string   "customer_shipping_province"
    t.string   "customer_shipping_zip"
    t.string   "customer_shipping_country"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "poll_categories", :force => true do |t|
    t.string   "name"
    t.string   "slug"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "poll_responses", :force => true do |t|
    t.integer  "poll_id",    :null => false
    t.integer  "user_id",    :null => false
    t.integer  "answer_id",  :null => false
    t.integer  "position",   :null => false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "polls", :force => true do |t|
    t.string   "question"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "active",           :default => false
    t.integer  "poll_category_id"
    t.boolean  "comments_closed"
    t.integer  "comments_count"
  end

  create_table "product_images", :force => true do |t|
    t.string   "product_image", :null => false
    t.integer  "product_id",    :null => false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "projects", :force => true do |t|
    t.string   "title"
    t.string   "slug"
    t.text     "body"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.date     "start_date"
    t.date     "end_date"
  end

  create_table "rates", :force => true do |t|
    t.integer "score"
  end

  create_table "ratings", :force => true do |t|
    t.integer  "user_id"
    t.integer  "rate_id"
    t.integer  "rateable_id"
    t.string   "rateable_type", :limit => 32
    t.text     "free_text"
    t.string   "rater_name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "ratings", ["rate_id"], :name => "index_ratings_on_rate_id"
  add_index "ratings", ["rateable_id", "rateable_type"], :name => "index_ratings_on_rateable_id_and_rateable_type"

  create_table "show_categories", :force => true do |t|
    t.string   "title"
    t.text     "description"
    t.string   "icon"
    t.string   "banner"
    t.string   "slug"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "display_order"
  end

  create_table "shows", :force => true do |t|
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "vimeo"
    t.string   "youtube"
    t.string   "thumbnail"
    t.integer  "show_category_id"
    t.string   "title"
    t.text     "description"
    t.boolean  "comments_closed"
    t.boolean  "staff_pick"
    t.string   "slug"
    t.integer  "impressions",      :default => 0
    t.integer  "comments_count"
    t.boolean  "featured",         :default => false
  end

  create_table "supporter_categories", :force => true do |t|
    t.string   "name"
    t.string   "slug"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "display_order"
  end

  create_table "supporters", :force => true do |t|
    t.string   "name"
    t.string   "address"
    t.string   "city"
    t.string   "state"
    t.string   "zip_code"
    t.string   "telephone"
    t.string   "website"
    t.string   "email"
    t.string   "logo"
    t.string   "ad"
    t.text     "description"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "supporter_category_id"
    t.integer  "display_order"
  end

  create_table "taggings", :force => true do |t|
    t.integer  "tag_id"
    t.integer  "taggable_id"
    t.string   "taggable_type"
    t.integer  "tagger_id"
    t.string   "tagger_type"
    t.string   "context"
    t.datetime "created_at"
  end

  add_index "taggings", ["tag_id"], :name => "index_taggings_on_tag_id"
  add_index "taggings", ["taggable_id", "taggable_type", "context"], :name => "index_taggings_on_taggable_id_and_taggable_type_and_context"

  create_table "tags", :force => true do |t|
    t.string "name"
  end

  create_table "users", :force => true do |t|
    t.string   "email",                                 :default => "",    :null => false
    t.string   "encrypted_password",     :limit => 128, :default => "",    :null => false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",                         :default => 0
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "admin",                                 :default => false
    t.boolean  "supplier",                              :default => false
    t.string   "first_name"
    t.string   "last_name"
    t.string   "avatar"
    t.boolean  "default_blogger",                       :default => false
    t.string   "twitter"
    t.string   "facebook"
    t.string   "tumbler"
    t.string   "wordpress"
    t.integer  "maker_id"
    t.string   "authentication_token"
    t.string   "flickr"
    t.string   "blogger"
    t.string   "pinterest"
  end

  add_index "users", ["authentication_token"], :name => "index_users_on_authentication_token", :unique => true
  add_index "users", ["email"], :name => "index_users_on_email", :unique => true
  add_index "users", ["reset_password_token"], :name => "index_users_on_reset_password_token", :unique => true

end
